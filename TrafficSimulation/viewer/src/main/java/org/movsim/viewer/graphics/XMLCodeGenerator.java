/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.viewer.graphics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 *
 * @author Andraz
 */
public class XMLCodeGenerator {
    private static int i;
    public static void main (String args[]) {
        i=3006;
        String code = "", codeAll = "";
        String str;
        StringTokenizer tokenizer;
        int i = 0;
        double a, b, c, d, angle, length;

        try {
            BufferedReader in = new BufferedReader(new FileReader("coordinates.txt"));
            while((str = in.readLine()) != null){
                tokenizer = new StringTokenizer(str);
                a = Double.parseDouble(tokenizer.nextToken());
                b = Double.parseDouble(tokenizer.nextToken());
                c = Double.parseDouble(tokenizer.nextToken());
                d = Double.parseDouble(tokenizer.nextToken());
                angle = getAngleBetweenTwoPoints(a, b, c, d);
                length = Math.sqrt((a-c)*(a-c) + (b-d)*(b-d));
                System.out.println("a="+a+" b="+b);
                codeAll += createJunctionCode(angle,a,b, length);
                i++;
            }

            //System.out.println(code);


            FileWriter fstream = new FileWriter("code.txt");
            BufferedWriter out = new BufferedWriter(fstream);

            //System.out.println(code);
            out.write(codeAll);
            out.close();
            } catch (IOException e) {
            System.out.println(e);
        }

    } 
    public static String createJunctionCode(double hdg,double a, double b, double length){
        
        String x="<road name=\"R"+ i +"\" length=\"19.5\" id=\""+ i +"\" junction=\"-1\">\n" +
"          <link>\n" +
"        </link>\n" +
"        <planView>\n" +
"            <geometry s=\"0.0\" x=\""+a+"\" y=\""+b+"\" hdg=\""+hdg+"\" length=\""+length+"\">\n" +
"                <line />\n" +
"            </geometry>\n" +
"        </planView>\n" +
"        <lanes>\n" +
"            <laneSection s=\"0.0\">\n" +
"                <right>\n" +
"                    <lane id=\"-1\" type=\"driving\" level=\"0\">\n" +
"                        <link>\n" +
"                            <predecessor id=\"-1\" />\n" +
"							<successor id=\"-1\" />\n" +
"                        </link>\n" +
"                        <width sOffset=\"0.0\" a=\"10.0\" b=\"0.0\" c=\"0.0\" d=\"0.0\" />\n" +
"                    </lane>\n" +
"                </right>\n" +
"            </laneSection>\n" +
"        </lanes>\n" +
"    </road>\n";
        i++;
        
        return x;
    }
    public static double getAngleBetweenTwoPoints(double a, double b,double c, double d){
        double xDiff = c - a; 
        double yDiff = b - d; 
        if(xDiff<0 && yDiff > 0)
            return (Math.PI) - Math.atan2(yDiff, Math.abs(xDiff)) ;
        else if(xDiff<0 && yDiff < 0)
            return (Math.PI) + Math.atan2(Math.abs(yDiff), Math.abs(xDiff)) ;
        else if(xDiff>0 && yDiff < 0)
            return 0 - Math.atan2(Math.abs(yDiff), Math.abs(xDiff));
        
        return Math.atan2(yDiff, xDiff);
    }
}
