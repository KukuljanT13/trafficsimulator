/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.viewer.ui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.Hashtable;
import java.util.ResourceBundle;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JToolBar;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.movsim.roadmappings.RoadMapping;
import org.movsim.roadmappings.RoadMappingArc;
import org.movsim.roadmappings.RoadMappingLine;
import org.movsim.simulator.Simulator;
import org.movsim.simulator.roadnetwork.RoadNetwork;
import org.movsim.simulator.roadnetwork.RoadSegment;
import org.movsim.viewer.graphics.TrafficCanvas;
import org.movsim.viewer.graphics.TrafficCanvasController;
import org.movsim.viewer.graphics.TrafficCanvasMouseListener;
import static org.movsim.viewer.ui.MovSimToolBar.logger;
import org.movsim.viewer.util.SwingHelper;

/**
 *
 * @author Andraz
 */
public class EditToolBar extends JToolBar implements ActionListener,ChangeListener { 

    JButton createNewRoad;
    JButton appendNewRoad;
    JButton connectRoad;
    JButton showPath;
    JButton setRoadWork;
    JButton applyButton;
    JButton cancelButton;
    
    ButtonGroup group;
    JRadioButton lineButton;
    JRadioButton arcButton;
    
    JSpinner posXSpinner;
    JSpinner posYSpinner;
    
    JSpinner startAngleSpinner;
    JSpinner arcAngleSpinner;
    JSpinner radiusSpinner;
    
    JLabel posXLabel;
    JLabel posYLabel;
    JLabel startAngleLabel;
    JLabel arcAngleLabel;
    JLabel radiusLabel;
            
    JSlider numberOfVehPerLenght;
    
    private JComboBox laneComboBox;
    private String [] laneComboSelection = new String[]{"1 lane","2 lanes","3 lanes","4 lanes", "5 lanes"};
    
    TrafficCanvas trafficCanvas;
    
    private boolean createMode = false;
    private boolean appendMode = false;
    private boolean pathMode = false;
    private boolean roadWorkMode = false;
    
    static final private String CREATE_NEW = "create new";
    static final private String APPEND = "append";
    static final private String CONNECT = "connect";
    static final private String PATH = "pathFind";
    static final private String ROADWORK = "roadWork";
    static final private String APPLY = "apply";
    static final private String CANCEL = "cancel";
    static final private String LINE = "line";
    static final private String ARC = "arc";
    
    private Point[] points = new Point[2];
    private double angle;
    
    private RoadSegment[] roadStack = new RoadSegment[2]; //For path finding
    
    private RoadSegment roadWork = null;
    
    
    public EditToolBar(ResourceBundle resourceBundle) {

       addButtons(this, resourceBundle);
    }
    
    protected void addButtons(JToolBar toolBar, ResourceBundle resourceBundle) {
        createNewRoad = createButton(resourceBundle, "button_create", CREATE_NEW, "Create", "Create");
        createNewRoad.setText("Create");
        toolBar.add(createNewRoad);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        appendNewRoad = createButton(resourceBundle, "button_append", APPEND, "Append", "Append");
        appendNewRoad.setText("Append");
        appendNewRoad.setEnabled(false);
        toolBar.add(appendNewRoad);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        connectRoad = createButton(resourceBundle, "button_append", CONNECT, "Append", "Append");
        connectRoad.setText("Connect");
        connectRoad.setEnabled(false);
        toolBar.add(connectRoad);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        showPath = createButton(resourceBundle, "button_path", PATH, "Find Path", "Find Path");
        showPath.setText("Find path");
        toolBar.add(showPath);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        setRoadWork = createButton(resourceBundle, "button_roadWork", ROADWORK, "Road Work", "Road Work");
        setRoadWork.setText("Set RoadWork");
        toolBar.add(setRoadWork);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        addSeparator();
        
        add(Box.createRigidArea(new Dimension(20,30)));
        laneComboBox = new JComboBox(laneComboSelection);
        laneComboBox.setSize(new Dimension(50, 30));
        laneComboBox.setEnabled(true);
        laneComboBox.setVisible(false);
        toolBar.add(laneComboBox);
        laneComboBox.setMaximumSize(new Dimension(80,30));
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        lineButton = new JRadioButton(LINE);
        lineButton.setActionCommand(LINE);
        lineButton.setSelected(true);
        lineButton.addActionListener(this);
        toolBar.add(lineButton);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        arcButton = new JRadioButton(ARC);
        arcButton.setActionCommand(ARC);
        arcButton.addActionListener(this);
        toolBar.add(arcButton);
        
        group = new ButtonGroup();
        group.add(lineButton);
        group.add(arcButton);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        
        posXLabel = new JLabel("X : ");
        posXLabel.setVisible(false);
        add(posXLabel);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        SpinnerModel modelPosX = new SpinnerNumberModel();
        posXSpinner = new JSpinner(modelPosX);
        posXSpinner.setMaximumSize(new Dimension(50,30));
        posXSpinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = (int)((JSpinner)e.getSource()).getValue();
                if (trafficCanvas.getCurrEditingRoadSegment() != null) {
                    RoadMapping rMapping = trafficCanvas.getCurrEditingRoadSegment().roadMapping();
                    if (rMapping instanceof RoadMappingArc) {
                        ((RoadMappingArc)rMapping).setXpos(value);
                    }
                    else if (rMapping instanceof RoadMappingLine) {
                        ((RoadMappingLine)rMapping).setXpos(value);
                    }

                    trafficCanvas.forceRepaintBackground();
                }
                
            }
        });
        posXSpinner.setVisible(false);
        toolBar.add(posXSpinner);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        posYLabel = new JLabel("Y : ");
        posYLabel.setVisible(false);
        add(posYLabel);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        SpinnerModel modelPosY = new SpinnerNumberModel();
        posYSpinner = new JSpinner(modelPosY);
        posYSpinner.setMaximumSize(new Dimension(50,30));
        posYSpinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                int value = (int)((JSpinner)e.getSource()).getValue();
                if (trafficCanvas.getCurrEditingRoadSegment() != null) {
                    RoadMapping rMapping = trafficCanvas.getCurrEditingRoadSegment().roadMapping();
                    if (rMapping instanceof RoadMappingArc) {
                        ((RoadMappingArc)rMapping).setYpos(value);
                    }
                    else if (rMapping instanceof RoadMappingLine) {
                        ((RoadMappingLine)rMapping).setYpos(value);
                    }

                    trafficCanvas.forceRepaintBackground();
                }
                
            }
        });
        posYSpinner.setVisible(false);
        toolBar.add(posYSpinner);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        startAngleLabel = new JLabel("Start Angle : ");
        startAngleLabel.setVisible(false);
        add(startAngleLabel);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        SpinnerModel modelStartAngle = new SpinnerNumberModel(0, //initial value
                               -360, //min
                               360, //max
                               0.5);                //step
        startAngleSpinner = new JSpinner(modelStartAngle);
        startAngleSpinner.setMaximumSize(new Dimension(50,30));
        startAngleSpinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                double valueInRad = (double)((JSpinner)e.getSource()).getValue()*(Math.PI/180);
                if (trafficCanvas.getCurrEditingRoadSegment() != null) {
                    RoadMapping rMapping = trafficCanvas.getCurrEditingRoadSegment().roadMapping();
                    if (rMapping instanceof RoadMappingArc) {
                        ((RoadMappingArc)rMapping).setStartAngle(valueInRad);
                    }
                    else if (rMapping instanceof RoadMappingLine) {
                        ((RoadMappingLine)rMapping).setStartAngle(valueInRad);
                    }

                    trafficCanvas.forceRepaintBackground();
                }
                
            }
        });
        startAngleSpinner.setVisible(false);
        toolBar.add(startAngleSpinner);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        arcAngleLabel = new JLabel("Arc Angle : ");
        arcAngleLabel.setVisible(false);
        add(arcAngleLabel);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        SpinnerModel modelArcAngle = new SpinnerNumberModel(180.0, //initial value
                               -360.0, //min
                               360.0, //max
                               1.0);
       
        arcAngleSpinner = new JSpinner(modelArcAngle);
        arcAngleSpinner.setMaximumSize(new Dimension(50,30));
        arcAngleSpinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                double valueInRad = (double)((JSpinner)e.getSource()).getValue()*(Math.PI/180);
                if (trafficCanvas.getCurrEditingRoadSegment() != null) {
                    RoadMapping rMapping = trafficCanvas.getCurrEditingRoadSegment().roadMapping();
                    if (rMapping instanceof RoadMappingArc) {
                        ((RoadMappingArc)rMapping).setArcAngle(valueInRad);
                        trafficCanvas.getCurrEditingRoadSegment().setRoadLength(((RoadMappingArc)rMapping).getRoadLength());
                    }
                    else {
                        System.out.println("Line doesnt have this option!!!");
                    }

                    trafficCanvas.forceRepaintBackground();
                }
                
            }
        });
        arcAngleSpinner.setVisible(false);
        toolBar.add(arcAngleSpinner);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        radiusLabel = new JLabel("Radius: ");
        radiusLabel.setVisible(false);
        add(radiusLabel);
        
        add(Box.createRigidArea(new Dimension(5,30)));
        
        SpinnerModel modelRadius = new SpinnerNumberModel(80.0, //initial value
                               0.0, //min
                               3600.0, //max
                               1.0);       
        radiusSpinner = new JSpinner(modelRadius);
        radiusSpinner.setMaximumSize(new Dimension(50,30));
        radiusSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                double value = (double)((JSpinner)e.getSource()).getValue();
                if (trafficCanvas.getCurrEditingRoadSegment() != null) {
                    RoadMapping rMapping = trafficCanvas.getCurrEditingRoadSegment().roadMapping();
                    if (rMapping instanceof RoadMappingArc) {
                        ((RoadMappingArc)rMapping).setRadius(value);
                        trafficCanvas.getCurrEditingRoadSegment().setRoadLength(((RoadMappingArc)rMapping).getRoadLength());
                    }
                    else {
                        System.out.println("Line doesnt have this option!!!");
                    }

                    trafficCanvas.forceRepaintBackground();
                }
                
            }
        });
        radiusSpinner.setVisible(false);
        toolBar.add(radiusSpinner);
         
         
        add(Box.createRigidArea(new Dimension(20,30)));
        
        numberOfVehPerLenght = new JSlider(JSlider.HORIZONTAL,0, 10, 5);
        numberOfVehPerLenght.addChangeListener(this);
        numberOfVehPerLenght.setMajorTickSpacing(10);
        numberOfVehPerLenght.setPaintTicks(true);
        numberOfVehPerLenght.setMaximumSize(new Dimension(150,50));
        //numberOfVehPerLenght.set
        
        //Create the label table
        Hashtable labelTable = new Hashtable();
        labelTable.put( new Integer( 0 ), new JLabel("Length") );
        labelTable.put( new Integer( 10 ), new JLabel("Congestion") );
        numberOfVehPerLenght.setLabelTable( labelTable );
        toolBar.add(numberOfVehPerLenght);
        
        numberOfVehPerLenght.setPaintLabels(true);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        applyButton =  createButton(resourceBundle, "button_apply", APPLY, "Apply", "Apply");
        applyButton.setText("Apply");
        applyButton.setVisible(false);
        toolBar.add(applyButton);
        
        add(Box.createRigidArea(new Dimension(20,30)));
        
        cancelButton = createButton(resourceBundle, "button_cancel", CANCEL, "Cancel", "Cancel");
        cancelButton.setText("Cancel");
        cancelButton.setVisible(false);
        toolBar.add(cancelButton);


    }    
        
    
    public void setTrafficCanvas(TrafficCanvas trafficCanvas) {
        this.trafficCanvas = trafficCanvas;
    }
    
    protected JButton createButton(ResourceBundle resourceBundle ,String imageName, String actionCommand, String toolTipResource, String textResource) {
        // Create and initialize the button.
        final JButton button = new JButton();
        button.setActionCommand(actionCommand);
        //button.setToolTipText(resourceBundle.getString(toolTipResource));
        button.addActionListener(this);

        return button;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String Action = e.getActionCommand();
        
        if (e.getActionCommand().equals(CREATE_NEW)) {
            System.out.println("I wana create a road xD");
            createMode = true;
            createNewRoad.setEnabled(false);
            laneComboBox.setVisible(true);
            applyButton.setVisible(true);
            cancelButton.setVisible(true);

        }
        if (e.getActionCommand().equals(APPEND)) {
            System.out.println("I wanna append road ");
            points[0] = new Point((int)trafficCanvas.getCurrEditingRoadSegment().roadMapping().endPos().x, (int) trafficCanvas.getCurrEditingRoadSegment().roadMapping().endPos().y);
            double startAngle = Math.toDegrees(trafficCanvas.getCurrEditingRoadSegment().roadMapping().endPos().theta());
            trafficCanvas.setColorToDefault(trafficCanvas.getCurrEditingRoadSegment());
            RoadSegment road = new RoadSegment(10, getNumberOfLanes());
            trafficCanvas.setCurrEditingRoadSegment(road);
            posXSpinner.setValue(points[0].x);
            posYSpinner.setValue(points[0].y);
            startAngleSpinner.setValue(startAngle);
            appendMode = true;
            laneComboBox.setVisible(true);
            applyButton.setVisible(true);
            cancelButton.setVisible(true);
        }
        if(e.getActionCommand().equals(CONNECT)){
            
            ConnectPanel cPanel = new ConnectPanel(trafficCanvas.getCurrEditingRoadSegment() , trafficCanvas.getSimulator().getRoadNetwork());
            int result = JOptionPane.showConfirmDialog(null, cPanel, 
               "Please Enter X and Y Values", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                cPanel.applyChanges(trafficCanvas);
            }
            cPanel = null;
            //System.out.println(cPanel.roadTo.getSelectedItem().toString());
        }
        if(e.getActionCommand().equals(CANCEL)){
            reset();
            repaint();
        }  
        if (e.getActionCommand().equals(APPLY)) {
            RoadSegment rs = trafficCanvas.getCurrEditingRoadSegment();
            //rs.roadMapping().setRoadColor(RoadMapping.defaultRoadColor());
            trafficCanvas.setColorToDefault(rs);
            trafficCanvas.getSimulator().getRoadNetwork().add(rs);
            reset();
        }
        if (e.getActionCommand().equals(ARC)) {
            arcAngleSpinner.setVisible(true);
            arcAngleLabel.setVisible(true);
            radiusSpinner.setVisible(true);
            radiusLabel.setVisible(true);
        }
        if (e.getActionCommand().equals(LINE)) {
            arcAngleSpinner.setVisible(false);
            arcAngleLabel.setVisible(false);
            radiusSpinner.setVisible(false);
            radiusLabel.setVisible(false);
        }
        if (e.getActionCommand().equals(PATH)) {
            pathMode = true;
            showPath.setEnabled(false);
            
        }
        if (e.getActionCommand().equals(ROADWORK)) {
            roadWorkMode = true;
            setRoadWork.setEnabled(false);
        }

        
    }
    
    public void pointCaptured(Point point){
        if (createMode || appendMode) {
            if (points[0]==null) {
                points[0]=point;
            } 
            else if(points[1]==null){
                points[1]=point;
            }
        }
        
    }  
    public void roadCaptured(RoadSegment road){
         if ( road == null) {
            roadStack[0] = null;
            roadStack[1] = null;

            roadWork=road;
            trafficCanvas.getSimulator().getRouting().setWeightMatrix(roadWork,(double)numberOfVehPerLenght.getValue()/(double)numberOfVehPerLenght.getMaximum());
            pathMode = false;
            showPath.setEnabled(true);
            roadWorkMode = false;
            setRoadWork.setEnabled(true);
            return;
        }
        if(roadWorkMode){
            roadWork = road;
            trafficCanvas.getSimulator().getRouting().setWeightMatrix(roadWork,(double)numberOfVehPerLenght.getValue()/(double)numberOfVehPerLenght.getMaximum());
            if (roadStack[0] != null && roadStack[1] != null) {
                trafficCanvas.giveMePath(roadStack[0].userId(), roadStack[1].userId(), trafficCanvas.getSimulator().getRouting().getWeightMatrix());
            }
            
                trafficCanvas.forceRepaintBackground();
            roadWorkMode = false;
            setRoadWork.setEnabled(true);
            return;
        }
        if(pathMode){
            if (roadStack[0] == null) {
                roadStack[0] = road;
                return;
            }
            else {
               roadStack[1] = road; 
            }

            trafficCanvas.getSimulator().getRouting().setWeightMatrix(roadWork,(double)numberOfVehPerLenght.getValue()/(double)numberOfVehPerLenght.getMaximum());
            trafficCanvas.giveMePath(roadStack[0].userId(), roadStack[1].userId(), trafficCanvas.getSimulator().getRouting().getWeightMatrix());
            //trafficCanvas.saveJPGImage("mojaslika");

            
            


        }
       
    
    }

    public boolean isRoadWorkMode() {
        return roadWorkMode;
    }

    
    public boolean isPathMode() {
        return pathMode;
    }

    public void setPathMode(boolean pathMode) {
        this.pathMode = pathMode;
    }
    
    public int getNumberOfLanes(){
        return laneComboBox.getSelectedIndex() + 1;
    }
    
    public RoadSegment calculateRoadMapping(Point one, Point two){
        double distance = Math.sqrt((one.x-two.x)*(one.x-two.x) + (one.y-two.y)*(one.y-two.y));

        RoadSegment road = trafficCanvas.getCurrEditingRoadSegment();
        if(road != null){
            road.setRoadLength(distance);
            road.setLaneCount(getNumberOfLanes());
        }
        else
            road = new RoadSegment(distance, getNumberOfLanes());
        
        RoadMapping roadMapping;
        double startAngle = 0;
        double arcAngle = 0;
        switch (group.getSelection().getActionCommand()) {
            case LINE:
                if(createMode){
                    roadMapping = new RoadMappingLine(10.0, getNumberOfLanes(), one.x, one.y, two.x, two.y);
                    startAngle = ((RoadMappingLine)roadMapping).getStartAngle()*(180/Math.PI);
                }else{
                    startAngle  = (double)startAngleSpinner.getValue();
                    roadMapping = new RoadMappingLine(getNumberOfLanes(), 0.0, one.x, one.y, Math.toRadians(startAngle), distance);
                    
                }
                posXSpinner.setValue(((RoadMappingLine)roadMapping).getXpos());
                posYSpinner.setValue(((RoadMappingLine)roadMapping).getYpos());
                road.setRoadMapping(roadMapping);
                break;
            case ARC:
                //roadMapping = new RoadMappingArc(10.0,  getNumberOfLanes(), one.x, one.y, two.x, two.y, 0.005);
                roadMapping = new RoadMappingArc(getNumberOfLanes(), one.x, one.y, distance, (double) startAngleSpinner.getValue()*(Math.PI/180), (double) arcAngleSpinner.getValue()*(Math.PI/180));
                startAngle  = ((RoadMappingArc)roadMapping).getStartAngle()*(180/Math.PI);
                arcAngle = ((RoadMappingArc)roadMapping).arcAngle()*(180/Math.PI);
               //((RoadMappingArc)roadMapping).setRadius(distance);
                road.setRoadMapping(roadMapping);
                arcAngleSpinner.setValue(arcAngle);
                radiusSpinner.setValue(distance);
                //((RoadMappingArc) roadMapping).setArcAngle(2);
                
                break;
            default:
                roadMapping = null;
        }
        startAngleSpinner.setValue(startAngle);
        setRoadValuesToSpinners(road);
        
        
        
        return road;
    }
    public boolean isCreateMode() {
        return createMode;
    }

    public boolean isAppendMode() {
        return appendMode;
    }
    
    public boolean isWaitingForEndPointCapture(){
        if (points[0] != null && points[1] == null) {
            return true;
        }
        return false;
    }

    public void setCreateMode(boolean createMode) {
        this.createMode = createMode;
    }

        
    public void setCreateNewBEnable(boolean bool){
        createNewRoad.setEnabled(bool);
    }
    public void setAppendBEnabled(boolean bool){
        appendNewRoad.setEnabled(bool);
    }
    public void setConnectBEnabled(boolean bool){
        connectRoad.setEnabled(bool);
    }
    public void setPathFindEnable(boolean  bool){
        showPath.setEnabled(bool);
    }
    public Point[] getPoints() {
        return points;
    }
    
    private void reset(){
        points = new Point[2];
        laneComboBox.setVisible(false);
        applyButton.setVisible(false);
        cancelButton.setVisible(false);
        trafficCanvas.setCurrEditingRoadSegment(null);
        createMode = false;
        appendMode = false;
        createNewRoad.setEnabled(true);
        trafficCanvas.forceRepaintBackground();
    }

   /* @Override
    public void stateChanged(ChangeEvent e) {
        SpinnerModel model = spinner.getModel();
        if (model instanceof SpinnerNumberModel) {
            double number = (double) model.getValue();
            System.out.println(number);
        }
    }*/
    
    public void setRoadValuesToSpinners(RoadSegment road){
        if (road == null) {
            posXSpinner.setValue(0);
            posYSpinner.setValue(0);
            posXSpinner.setVisible(false);
            posXLabel.setVisible(false);
            posYSpinner.setVisible(false);
            posYLabel.setVisible(false);
            startAngleSpinner.setVisible(false);
            startAngleLabel.setVisible(false);
            arcAngleSpinner.setVisible(false);
            arcAngleLabel.setVisible(false);
            radiusSpinner.setVisible(false);
            radiusLabel.setVisible(false);
            return;
        }
        //createMode = true;
        RoadMapping mapping = road.roadMapping();
        if (mapping instanceof RoadMappingLine) {
            RoadMappingLine mappingLine = (RoadMappingLine)mapping;
            posXSpinner.setValue(mappingLine.getXpos());
            posYSpinner.setValue(mappingLine.getYpos());
            startAngleSpinner.setValue(mappingLine.getStartAngle()*(180/Math.PI));
            group.setSelected(lineButton.getModel(), true);
            posXSpinner.setVisible(true);
            posXLabel.setVisible(true);
            posYSpinner.setVisible(true);
            posYLabel.setVisible(true);
            startAngleSpinner.setVisible(true);
            startAngleLabel.setVisible(true);
            arcAngleSpinner.setVisible(false);
            arcAngleLabel.setVisible(false);
            radiusSpinner.setVisible(false);
            radiusLabel.setVisible(false);
        }else{
            RoadMappingArc mappingArc = ((RoadMappingArc)mapping);
            posXSpinner.setValue(mappingArc.getXpos());
            posYSpinner.setValue(mappingArc.getYpos());
            startAngleSpinner.setValue(mappingArc.getStartAngle()*(180/Math.PI));
            group.setSelected(arcButton.getModel(), true);
            posXSpinner.setVisible(true);
            posXLabel.setVisible(true);
            posYSpinner.setVisible(true);
            posYLabel.setVisible(true);
            startAngleLabel.setVisible(true);
            startAngleSpinner.setVisible(true);
            arcAngleSpinner.setVisible(true);
            arcAngleLabel.setVisible(true);
            radiusSpinner.setVisible(true);
            radiusLabel.setVisible(true);
            arcAngleSpinner.setValue(Math.toDegrees(mappingArc.getArcAngle()));
            
            radiusSpinner.setValue(mappingArc.getRadius());
        }
        
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        JSlider source = (JSlider)e.getSource();
        if (!source.getValueIsAdjusting()) {
            trafficCanvas.getSimulator().getRouting().setWeightMatrix(roadWork,(double)source.getValue()/(double)source.getMaximum());
            if (roadStack[0] != null && roadStack[1] != null) {
                trafficCanvas.giveMePath(roadStack[0].userId(), roadStack[1].userId(), trafficCanvas.getSimulator().getRouting().getWeightMatrix());
            }
            
            trafficCanvas.forceRepaintBackground();
        }
    }
}
