
import java.io.*;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vida
 */
public class junctionXML {
    public static void main (String args[]) {
        String[] code = {"",""}; 
        //String codeAll = "";
        String str;
        StringTokenizer tokenizer;
        int i = 0;
        double a, b;
        
        try {
            BufferedReader in = new BufferedReader(new FileReader("coordinates.txt"));
            while((str = in.readLine()) != null){
                tokenizer = new StringTokenizer(str);
                a = Double.parseDouble(tokenizer.nextToken());
                b = Double.parseDouble(tokenizer.nextToken());
                System.out.println("a="+a+" b="+b);
                code[0] += createJunctionCode(i, a, b)[0];
                code[1] += createJunctionCode(i, a, b)[1];
                //codeAll += code[0];
                i++;
            }

            //System.out.println(code);
        
            FileWriter fstream = new FileWriter("speedlimit.xodr", true);
            BufferedWriter out = new BufferedWriter(fstream);
            
            //System.out.println(code);
            //out.write(codeAll);
            out.write(code[0]);
            out.write(code[1]);
            out.write("</OpenDRIVE>");
            out.close();
            } catch (IOException e) {
            System.out.println(e);
        }
        
    } 
    
    public static String[] createJunctionCode(int junctionNumber, double a, double b){
        String[] code = {"",""}, codeAll = {"",""}; 
        int junction_outerR = 30; 
        int junction1 = 12, junction2 = 25, junction3 = 37;
        
        //System.out.println("a="+a+" b="+b);
        
        String[] roadName = new String[20]; 
        String[] x = new String[20];
        String[] y = new String[20];
        String[] hdg = new String[20];
        String[] l = new String[20];
        String[] arc = new String[20];
        String[] speed = new String[20];
        String[] successor = new String[20];
        String[] predecessor = new String[20];
        String[] suc = new String[20];
        String[] pre = new String[20];
        String[] signal = new String[20];
        
        for(int i = 0; i < 20; i++){
            speed[i] = "";
            successor[i] = "";
            predecessor[i] = "";
            suc[i] = "";
            pre[i] = "";
            signal[i] = "";
        }
        
        
        //signals
        String[] origin = {"west", "south", "east", "north"};
        String[] signalName = {"first", "third", "second", "fourth"};
        codeAll[1] += "    <controller id=\"plan1\" >\n";
        for(int i = 0; i < 4; i++){
            signal[i] = "<signals>\n" +
"	\t<signal s=\""+(junction_outerR-5)+"\" id=\"r1_"+origin[i]+"_"+(i+20*junctionNumber)+"\" name=\""+signalName[i]+"\" /> \n" +
"\t</signals>";
            codeAll[1] +=
"        <control signalId=\"r1_"+origin[i]+"_"+(i+20*junctionNumber)+"\"/>\n"; 

        }
        codeAll[1] += "    </controller>\n";
        
        roadName[0] = "<!-- WEST to JUNCTION -->";
        x[0] = "" + (a-junction_outerR);
        y[0] = "" + (b+junction2);
        hdg[0] = "" + 0;
        l[0] = "" + junction_outerR;
        arc[0] = "<line />";
        //speed[0] = "<speed sOffset=\"20\" max=\"13.89\" />";
        
        roadName[1] = "<!-- SOUTH to JUNCTION -->";
        x[1] = "" + (a+junction2);
        y[1] = "" + (b+junction_outerR+junction3);
        hdg[1] = "" + 1.5708;
        l[1] = "" + junction_outerR;
        arc[1] = "<line />";
        
        roadName[2] = "<!-- EAST to JUNCTION -->";
        x[2] = "" + (a+junction_outerR+junction3);
        y[2] = "" + (b+junction1);
        hdg[2] = "" + 3.14159;
        l[2] = "" + junction_outerR;
        arc[2] = "<line />";
        //speed[1] = "<speed sOffset=\"20\" max=\"13.89\" />";
        
        roadName[3] = "<!-- NORTH to JUNCTION -->";
        x[3] = "" + (a+junction1);
        y[3] = "" + (b-junction_outerR);
        hdg[3] = "" + (-1.5708);
        l[3] = "" + junction_outerR;
        arc[3] = "<line />";
        
        
        roadName[4] = "<!-- JUNCTION to WEST -->";
        x[4] = "" + a;
        y[4] = "" + (b+junction1);
        hdg[4] = "" + 3.14159;
        l[4] = "" + junction_outerR;
        arc[4] = "<line />";
        
        roadName[5] = "<!-- JUNCTION to SOUTH -->";
        x[5] = "" + (a+junction1);
        y[5] = "" + (b+junction3);
        hdg[5] = "" + (-1.5708);
        l[5] = "" + junction_outerR;
        arc[5] = "<line />";
        
        roadName[6] = "<!-- JUNCTION to EAST -->";
        x[6] = "" + (a+junction3);
        y[6] = "" + (b+junction2);
        hdg[6] = "" + 0;
        l[6] = "" + junction_outerR;
        arc[6] = "<line />";
        
        roadName[7] = "<!-- JUNCTION to NORTH -->";
        x[7] = "" + (a+junction2);
        y[7] = "" + b;
        hdg[7] = "" + 1.5708;
        l[7] = "" + junction_outerR;
        arc[7] = "<line />";
        

        for(int i = 8; i < 20; i++){
            pre[i] = "<predecessor id=\"-1\" />";
            suc[i] = "<successor id=\"-1\" />";
        }
        
        roadName[8] = "<!-- WEST to EAST -->";
        x[8] = "" + a;
        y[8] = "" + (b+junction2);
        hdg[8] = "" + 0;
        l[8] = "" + 37;
        arc[8] = "<line />";
        successor[8] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+6)+"\" contactPoint=\"end\"/>";
        predecessor[8] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20)+"\" contactPoint=\"end\" />"; 
        
        roadName[9] = "<!-- WEST to NORTH -->";
        x[9] = "" + a;
        y[9] = "" + (b+junction2);
        hdg[9] = "" + 0;
        l[9] = "" + 39.25;
        arc[9] = "<arc curvature = \"0.04\" />";
        successor[9] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+7)+"\" contactPoint=\"end\"/>";
        predecessor[9] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20)+"\" contactPoint=\"end\" />";
        
        roadName[10] = "<!-- WEST to SOUTH -->";
        x[10] = "" + a;
        y[10] = "" + (b+junction2);
        hdg[10] = "" + 0;
        l[10] = "" + 19.5;
        arc[10] = "<arc curvature = \"-0.08\" />";
        successor[10] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+5)+"\" contactPoint=\"end\"/>";
        predecessor[10] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20)+"\" contactPoint=\"end\" />";
        
        roadName[11] = "<!-- SOUTH to NORTH -->";
        x[11] = "" + (a+junction2);
        y[11] = "" + (b+junction3);
        hdg[11] = "" + 1.5708;
        l[11] = "" + 37;
        arc[11] = "<line />";
        successor[11] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+1+6)+"\" contactPoint=\"end\"/>";
        predecessor[11] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+1)+"\" contactPoint=\"end\" />";
        
        roadName[12] = "<!-- SOUTH to WEST -->";
        x[12] = "" + (a+junction2);;
        y[12] = "" + (b+junction3);
        hdg[12] = "" + 1.5708;
        l[12] = "" + 39.25;
        arc[12] = "<arc curvature = \"0.04\" />";
        successor[12] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+1+3)+"\" contactPoint=\"end\"/>";
        predecessor[12] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+1)+"\" contactPoint=\"end\" />";
        
        roadName[13] = "<!-- SOUTH to EAST -->";
        x[13] = "" + (a+junction2);
        y[13] = "" + (b+junction3);
        hdg[13] = "" + 1.5708;
        l[13] = "" + 19.5;
        arc[13] = "<arc curvature = \"-0.08\" />";
        successor[13] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+1+5)+"\" contactPoint=\"end\"/>";
        predecessor[13] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+1)+"\" contactPoint=\"end\" />";
        
        roadName[14] = "<!-- EAST to WEST -->";
        x[14] = "" + (a+junction3);
        y[14] = "" + (b+junction1);
        hdg[14] = "" + 3.14159;
        l[14] = "" + 37;
        arc[14] = "<line />";
        successor[14] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+2+2)+"\" contactPoint=\"end\"/>";
        predecessor[14] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+2)+"\" contactPoint=\"end\" />";
        
        roadName[15] = "<!-- EAST to SOUTH -->";
        x[15] = "" + (a+junction3);
        y[15] = "" + (b+junction1);
        hdg[15] = "" + 3.14159;
        l[15] = "" + 39.25;
        arc[15] = "<arc curvature = \"0.04\" />";
        successor[15] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+2+3)+"\" contactPoint=\"end\"/>";
        predecessor[15] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+2)+"\" contactPoint=\"end\" />";
        
        roadName[16] = "<!-- EAST to NORTH -->";
        x[16] = "" + (a+junction3);
        y[16] = "" + (b+junction1);
        hdg[16] = "" + 3.14159;
        l[16] = "" + 19.5;
        arc[16] = "<arc curvature = \"-0.08\" />";
        successor[16] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+2+5)+"\" contactPoint=\"end\"/>";
        predecessor[16] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+2)+"\" contactPoint=\"end\" />";
        
        roadName[17] = "<!-- NORTH to SOUTH -->";
        x[17] = "" + (a+junction1);
        y[17] = "" + b;
        hdg[17] = "" + (-1.5708);
        l[17] = "" + 37;
        arc[17] = "<line />";
        successor[17] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+5)+"\" contactPoint=\"end\"/>";
        predecessor[17] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+3)+"\" contactPoint=\"end\" />";
        
        roadName[18] = "<!-- NORTH to EAST -->";
        x[18] = "" + (a+junction1);
        y[18] = "" + b;
        hdg[18] = "" + (-1.5708);
        l[18] = "" + 39.25;
        arc[18] = "<arc curvature = \"0.04\" />";
        successor[18] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+6)+"\" contactPoint=\"end\"/>";
        predecessor[18] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+3)+"\" contactPoint=\"end\" />";
        
        roadName[19] = "<!-- NORTH to WEST -->";
        x[19] = "" + (a+junction1);
        y[19] = "" + b;
        hdg[19] = "" + (-1.5708);
        l[19] = "" + 19.5;
        arc[19] = "<arc curvature = \"-0.08\" />"; 
        successor[19] = "<successor elementType=\"road\" elementId=\""+(junctionNumber*20+4)+"\" contactPoint=\"end\"/>";
        predecessor[19] = "<predecessor elementType=\"road\" elementId=\""+(junctionNumber*20+3)+"\" contactPoint=\"end\" />";
        
        codeAll[0] += "<!-- Junction " + junctionNumber + "-->\n";
        for(int i = 0; i < 20; i++){
            code[0] = roadName[i] + "\n" +
                "<road name=\"R"+ (junctionNumber*20 + i) +"\" length=\""+l[i]+"\" id=\""+ (junctionNumber*20 + i) + "\" junction=\"-1\"> " + "\n"
                    +"\t <link>" + "\n"
                        +"\t\t" + predecessor[i] + "\n"
                        +"\t\t" + successor[i] + "\n"
                    +"\t </link>" + "\n"
                    +"\t <planView>" + "\n"
                        +"\t\t <geometry s=\"0.0\" x=\""+x[i]+"\" y=\""+y[i]+"\" hdg=\""+hdg[i]+"\" length=\""+l[i]+"\">" + "\n"
                            +"\t\t\t"+ arc[i] + "\n"
                        +"\t\t </geometry>" + "\n"
                    +"\t </planView>" + "\n"
                    +"\t <lanes>" + "\n"
                        +"\t\t <laneSection s=\"0.0\">" + "\n"
                            +"\t\t\t <right>" + "\n"
                                +"\t\t\t\t <lane id=\"-1\" type=\"driving\" level=\"0\">" + "\n"
                                    +"\t\t\t\t <link>" + "\n"
                                        +"\t\t\t\t\t" + pre[i] + "\n"
                                        +"\t\t\t\t\t" + suc[i] + "\n"
                                    +"\t\t\t\t </link>" + "\n"
                                    +"\t\t\t\t <width sOffset=\"0.0\" a=\"10.0\" b=\"0.0\" c=\"0.0\" d=\"0.0\" />" + "\n"
                                            +"\t\t\t\t\t" + speed[i] + "\n"
                                +"\t\t\t\t </lane>" + "\n"
                            +"\t\t\t </right>" + "\n"
                        +"\t\t </laneSection>" + "\n"
                    +"\t </lanes>" + "\n"
                    + "\t\t" + signal[i] + "\n"                           
                +"</road>" + "\n";
            
            codeAll[0] += code[0];
                }
        
        
        //System.out.println(codeAll);
        return codeAll;
        }
        
        
}
