/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.viewer.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.movsim.simulator.Simulator;
import org.movsim.simulator.roadnetwork.LaneSegment;
import org.movsim.simulator.roadnetwork.Link;
import org.movsim.simulator.roadnetwork.RoadNetwork;
import org.movsim.simulator.roadnetwork.RoadSegment;
import org.movsim.viewer.graphics.TrafficCanvas;

/**
 *
 * @author Andraz
 */
public class ConnectPanel extends JPanel implements ActionListener{

    RoadSegment roadToConnect;
    
    //UI for Successor
    JLabel labelSuccessor;
    public JComboBox roadTo;
    
    JLabel[] succLanes;
    JComboBox[] succBoxes;
    
    //UI for Predecessor
    JLabel labelPredecessor;
    JComboBox roadFrom;
    
    JLabel[] predLanes;
    JComboBox[] predBoxes;
    
    
    public ConnectPanel(final RoadSegment roadToConnect, RoadNetwork roadNetwork) {
        this.roadToConnect = roadToConnect;
        
        // UI FOR SUCCESSOR
        labelSuccessor = new JLabel("Choose successor : ");
        add(labelSuccessor, BorderLayout.CENTER);
        
        Iterator<RoadSegment> iter = roadNetwork.iterator();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement(null);
        while (iter.hasNext()) {
            RoadSegment roadSegment = iter.next();
            model.addElement(roadSegment);   
        }
        
        roadTo = new JComboBox(model);
        roadTo.addActionListener(new ActionListener () {
            @Override
            public void actionPerformed(ActionEvent e) {             
                JComboBox comBox = (JComboBox)e.getSource();
                RoadSegment x = (RoadSegment)comBox.getSelectedItem();
                //roadToConnect.adds
                if (x!=null) {
                    updateDialogPanel(x, false);
                }
                //
                System.out.println(x);
            }
        });
        add(roadTo,BorderLayout.CENTER);
       // if(roadToConnect.getSourceRoadSegments().size())
        int numberOfSuccessorRoads = roadToConnect.getSinkRoadSegments().size();
        if (numberOfSuccessorRoads == 0) {
            generateSuccessorInput(null);
            roadTo.setSelectedIndex(-1);   
        }else if (numberOfSuccessorRoads == 1) {
            RoadSegment succesorRoad = (RoadSegment)roadToConnect.getSinkRoadSegments().toArray()[0];
            Object[] selection = createOptions(succesorRoad);
            generateSuccessorInput(selection);
            roadTo.setSelectedItem(succesorRoad);
            
            
            
        }else
            roadTo.setEnabled(false);
        
        //UI FOR PREDECESSOR
        labelPredecessor = new JLabel("Choose predecessor : ");
        add(labelPredecessor, BorderLayout.CENTER);
        
        iter = roadNetwork.iterator();
        model = new DefaultComboBoxModel();
        while (iter.hasNext()) {
            RoadSegment roadSegment = iter.next();
            model.addElement(roadSegment);   
        }
        
        roadFrom = new JComboBox(model);
        roadFrom.addActionListener(new ActionListener () {
            @Override
            public void actionPerformed(ActionEvent e) {             
                JComboBox comBox = (JComboBox)e.getSource();
                RoadSegment roadSelected = (RoadSegment)comBox.getSelectedItem();
                //roadToConnect.adds
                if(roadSelected != null)
                    updateDialogPanel(roadSelected, true);

            }
        });
        //roadFrom.addActionListener(this);
        add(roadFrom,BorderLayout.CENTER);
       // if(roadToConnect.getSourceRoadSegments().size())
        int numberOfPredecessorRoads = roadToConnect.getSourceRoadSegments().size();
        if (numberOfPredecessorRoads == 0) {
            generatePredecessorInput(null);
            roadFrom.setSelectedIndex(-1);
            
        }else if (numberOfPredecessorRoads == 1) {
            RoadSegment predecessorRoad = (RoadSegment)roadToConnect.getSourceRoadSegments().toArray()[0];
            Object[] selection = createOptions(predecessorRoad);
            generatePredecessorInput(selection);
            roadFrom.setSelectedItem(predecessorRoad);
            
            
        }else
            roadFrom.setEnabled(false);
        setSelectedIndexes();
        this.setLayout(new GridLayout(0,2));
        
        
        
        
        
        //layout.setAutoCreateGaps(true);
        //layout.setAutoCreateContainerGaps(true);
        
        //layout.setHorizontalGroup(
   //layout.createSequentialGroup()
     // .addComponent(labelSuccessor)
    //  .addComponent(roadTo)
//);
    }

    private void updateDialogPanel(RoadSegment road, boolean isPredecessor){
        if (isPredecessor) {
            Object[] options = createOptions(road);
            /*if(predBoxes==null){
                generatePredecessorInput(options);
            }*/
            for (int i = 0; i < predBoxes.length; i++) {
                predBoxes[i].removeAllItems();
                predBoxes[i].setModel(new DefaultComboBoxModel(options));
            }
        }else{
            Object[] options = createOptions(road);
            /*if(succBoxes==null)
                generateSuccessorInput(options);*/
            for (int i = 0; i < succBoxes.length; i++) {
                succBoxes[i].removeAllItems();
                succBoxes[i].setModel(new DefaultComboBoxModel(options));
            }
        }
    }
    
    /*private String[] createSelection(RoadSegment road){
        int size = road.laneSegments.length + 1; //plus 1 for unselected!
        String[] selection = new String[size];
        selection[0] = "undefined";
        for (int i = 1; i < size; i++) {
            selection[i] = "Lane " + i;
        }
        return selection;
        
    }*/
    private Object[] createOptions(RoadSegment road){
        if (road == null) {
            System.out.println("road je null!!");
            return null;
        }
        Object[] options = new Object[road.laneSegments.length + 1];
        options[0] = "undefined";
        int i = 1;
        for (LaneSegment lane : road.laneSegments) {
            options[i] = lane;
            i++;
        }
        return options;
    }
    private void generateSuccessorInput(Object[] selection){
        int numberOfLaneSegments = roadToConnect.laneSegments.length;
        
        succLanes = new JLabel[numberOfLaneSegments];
        succBoxes = new JComboBox[numberOfLaneSegments];
        for (int i = 0; i < succLanes.length; i++) {
            succLanes[i] = new JLabel("Lane" + (i+1));
            add(succLanes[i]);
            if (selection == null) {
                succBoxes[i] = new JComboBox();
            }else
                succBoxes[i] = new JComboBox(selection);
            
            //succBoxes[i].setActionCommand("Lane" + (i+1));
           
            add(succBoxes[i]);
            
        }
    }
    
    private void generatePredecessorInput(Object[] selection){
        int numberOfLaneSegments = roadToConnect.laneSegments.length;
        
        predLanes = new JLabel[numberOfLaneSegments];
        predBoxes = new JComboBox[numberOfLaneSegments];
        for (int i = 0; i < predLanes.length; i++) {
            predLanes[i] = new JLabel("Lane" + (i+1));
            add(predLanes[i]);
            if (selection == null) {
                predBoxes[i] = new JComboBox();
            }else
            predBoxes[i] = new JComboBox(selection);
           
            add(predBoxes[i]);
        }
    }
   /*private void generateSuccessorGUI(){
        if (roadToConnect.getSinkRoadSegments().size()) {
           
        }
        for (LaneSegment lane : roadToConnect.laneSegments) {
            String[] selection = createSelection(lane.)
        }
    }*/

    private void setSelectedIndexes(){
        for (int i = 0; i < succLanes.length; i++) {
            if (roadToConnect.laneSegments[i].sinkLaneSegments.size() != 0) {
                succBoxes[i].setSelectedIndex(((LaneSegment)roadToConnect.laneSegments[i].sinkLaneSegments.toArray()[0]).lane());
            }
        }
        
        for (int i = 0; i < predLanes.length; i++) {
            if (roadToConnect.laneSegments[i].sourceLaneSegments.size() != 0) {
                predBoxes[i].setSelectedIndex(((LaneSegment)roadToConnect.laneSegments[i].sourceLaneSegments.toArray()[0]).lane());
            }
        }
    }
    
    public void applyChanges(TrafficCanvas trafficCanvas){
        HashSet sinkSegment = new HashSet<>();
        sinkSegment.add(roadTo.getSelectedItem());
        roadToConnect.setSinkRoadSegments(sinkSegment);
        HashSet sourceSegment = new HashSet<>();
        sourceSegment.add(roadFrom.getSelectedItem());
        roadToConnect.setSourceRoadSegments(sourceSegment);
        for (int i = 0; i < roadToConnect.laneSegments.length; i++) {
            roadToConnect.laneSegments[i].setSinkLaneSegments(new HashSet<LaneSegment>());
            //roadToConnect.laneSegments[i].setSourceLaneSegment(null);
        }
        
       // roadToConnect.laneSegments[0].get
        Object successor = roadTo.getSelectedItem();
        if (successor != null) {
            for (int i = 0; i < roadToConnect.laneCount(); i++){
            //roadToConnect.laneSegments = new LaneSegment[roadToConnect.laneCount()];
             Link.addLanePair(roadToConnect.laneSegments[i].lane(), roadToConnect, ((LaneSegment)succBoxes[i].getSelectedItem()).lane(), (RoadSegment) successor);
            }
        }
        
        
        Object predecessor = roadFrom.getSelectedItem();
        if (predecessor != null) {
            for (int i = 0; i < roadToConnect.laneCount(); i++) {
                Link.addLanePair(((LaneSegment)predBoxes[i].getSelectedItem()).lane(),(RoadSegment) predecessor, roadToConnect.laneSegments[i].lane(), roadToConnect);
            }
        }
        
        //trafficCanvas.reset();
        System.out.println("jerigjeiarg");
           
        
       
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        /*switch(e.getActionCommand()){
            case "Lane 1":
                this.roadToConnect.laneSegments[0].sinkLaneSegments = new HashSet();
                Link.addLanePair(roadToConnect.laneSegments[0].lane(), roadToConnect, ((LaneSegment)succBoxes[0].getSelectedItem()).lane(), (RoadSegment)roadTo.getSelectedItem());
            break;
            case
        }*/
    }
    
}
