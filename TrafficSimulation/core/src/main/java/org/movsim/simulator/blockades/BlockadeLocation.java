/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.simulator.blockades;

import com.google.common.base.Preconditions;

/**
 *
 * @author Andraz
 */
public class BlockadeLocation {
    
    private Blockade blockade;

    private final double position;
    
    private boolean wasSetByAnotherCriticalInterval = false;
    
    

    public BlockadeLocation(double position, Blockade blockade) {
        this.position = position;
        this.blockade = blockade;
    }


    /**
     * Returns the position on the road segment.
     * 
     * @return the position (m)
     */
    public double position() {
        return position;
    }

    public Blockade getBlockade() {
        Preconditions.checkNotNull(blockade,
                "blockade not set.");
        return blockade;
    }

    public void setBlockade(Blockade blockade) {
        Preconditions.checkArgument(this.blockade == null);
        this.blockade = blockade;
    }

    @Override
    public String toString() {
        return "BlockadeLocation [position = "
                + position + ", blockade=" + blockade + "]";
    }
    
    public void reset(){
        wasSetByAnotherCriticalInterval = false;
        blockade.reset();
    }


}
