/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.simulator.vehicles.longitudinalmodel;

import org.movsim.simulator.MovsimConstants;
import org.movsim.simulator.blockades.Blockade;
import org.movsim.simulator.roadnetwork.RoadSegment.BlockadeLocationWithDistance;
import org.movsim.simulator.vehicles.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Andraz
 * The class BlockadeApproaching
 */
public class BlockadeApproaching  {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(BlockadeApproaching.class);

    public static final double MAX_LOOK_AHEAD_DISTANCE = 1000;

    private boolean considerBlockade;

    private double accBlockade;

    private double distanceToBlockade;

    /**
     * Instantiates a new blockade approaching.
     */
    public BlockadeApproaching() {
        considerBlockade = false;
        distanceToBlockade = MovsimConstants.INVALID_GAP;
    }

    /**
     * Update.
     * 
     * @param me
     * @param blockade
     * @param distanceToBlockade
     * @param longModel
     */
    public void update(Vehicle me, Blockade blockade, double distanceToBlockade) {
        accBlockade = 0;
        considerBlockade = false;

        if (distanceToBlockade > MAX_LOOK_AHEAD_DISTANCE) {
            LOG.debug("blockade at distance={} to far away -- MAX_LOOK_AHEAD_DISTANCE={}", distanceToBlockade,
                    MAX_LOOK_AHEAD_DISTANCE);
            return;
        }

        /*if (blockade.status() == false && me.getLength() > 30
                && distanceToBlockade < 0.5 * MAX_LOOK_AHEAD_DISTANCE) {
            // special case here: only relevant if vehicle is really long and next blockade is quite close
            //checkSpaceBeforePassingTrafficlight(me, blockade, distanceToBlockade);TODO
        } else */if (blockade.status() != false) {
            final double maxRangeOfSight = MovsimConstants.GAP_INFINITY;
            if (distanceToBlockade< maxRangeOfSight) {
                accBlockade = calcAccelerationToBlockade(me, distanceToBlockade);
                if (accBlockade < 0) {
                    considerBlockade = true;
                    LOG.debug("distance to blockadet = {}, accTL = {}", distanceToBlockade, accBlockade);
                }

                // blockade is already red
                if (blockade.status() == true) {
                    final double maxDeceleration = me.getMaxDeceleration();
                    final double minBrakeDist = (me.getSpeed() * me.getSpeed()) / (2 * maxDeceleration);
                    if (accBlockade <= -maxDeceleration || minBrakeDist >= distanceToBlockade) {
                        // ignore blockade
                        LOG.info(String
                                .format("veh id=%d in dilemma zone is going to pass blockade at distance=%.2fm due to physics (assuming user-defined max. possible braking=%.2fm/s^2!",
                                        me.getId(), distanceToBlockade, maxDeceleration));
                        considerBlockade = false;
                    }
                }
            }
        }
    }

    private static double calcAccelerationToBlockade(Vehicle me, double distanceToBlockade) {
        final double speed = me.getSpeed();
        return Math.min(0, me.getLongitudinalModel().calcAccSimple(distanceToBlockade, speed, speed));
    }

    /**
     * Consider blockade.
     * 
     * @return true, if successful
     */
    public boolean considerBlockade() {
        return considerBlockade;
    }

    /**
     * Acc approaching.
     * 
     * @return the double
     */
    public double accApproaching() {
        return accBlockade;
    }

    /**
     * Gets the distance to blockade.
     * 
     * @return the distance to blockade
     */
    public double getDistanceToBlockade() {
        return distanceToBlockade;
    }

    private void checkSpaceBeforePassingBlockade(Vehicle me, Blockade blockade,
            double distanceToBlockade) {
        // relative to position of first traffic light
        BlockadeLocationWithDistance nextBlockade = blockade.roadSegment().getNextDownstreamBlockade(
                blockade.position(), me.lane(), MAX_LOOK_AHEAD_DISTANCE);
        if (nextBlockade != null) {
            double distanceBetweenBlockades = nextBlockade.distance;
            if (distanceBetweenBlockades < 0.5 * MAX_LOOK_AHEAD_DISTANCE) {
                double effectiveFrontVehicleLengths = calcEffectiveFrontVehicleLengths(me, blockade,
                        distanceToBlockade + distanceBetweenBlockades);
                LOG.debug("distanceBetweenBlockades={}, effectiveLengths+ownLength={}",
                        distanceBetweenBlockades, effectiveFrontVehicleLengths + me.getEffectiveLength());
                if (effectiveFrontVehicleLengths > 0
                        && distanceBetweenBlockades < effectiveFrontVehicleLengths + me.getEffectiveLength()) {
                    considerBlockade = true;
                    accBlockade = calcAccelerationToBlockade(me, distanceToBlockade);
                    LOG.debug(
                            "stop in front of green trafficlight, not sufficient space: nextlight={}, space for vehicle(s)={}",
                            distanceBetweenBlockades, effectiveFrontVehicleLengths + me.getEffectiveLength());
                }
            }
        }
    }

    private static double calcEffectiveFrontVehicleLengths(Vehicle me, Blockade blockade,
            double distanceToSecondTrafficlight) {
        double sumEffectiveLengths = 0;
        Vehicle frontVehicle = blockade.roadSegment().laneSegment(me.lane()).frontVehicle(me);
        while (frontVehicle != null && me.getBrutDistance(frontVehicle) < distanceToSecondTrafficlight) {
            sumEffectiveLengths += frontVehicle.getEffectiveLength();
            Vehicle prevFront = frontVehicle;
            frontVehicle = blockade.roadSegment().laneSegment(frontVehicle.lane()).frontVehicle(frontVehicle);
            if (frontVehicle != null && prevFront.getId() == frontVehicle.getId()) {
                // FIXME seems to be a real bug: get back the *same* vehicle when its entered the downstream roadsegment
                break;
            }
        }
        return sumEffectiveLengths;
    }
}
