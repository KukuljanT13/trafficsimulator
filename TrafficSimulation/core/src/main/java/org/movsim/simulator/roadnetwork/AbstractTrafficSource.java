package org.movsim.simulator.roadnetwork;

import com.google.common.base.Preconditions;
import java.util.ArrayList;
import java.util.Random;
import javax.annotation.Nullable;
import org.movsim.input.ProjectMetaData;
import org.movsim.simulator.SimulationTimeStep;
import org.movsim.simulator.roadnetwork.routing.Route;
import org.movsim.simulator.roadnetwork.routing.Routing;
import org.movsim.simulator.vehicles.TestVehicle;
import org.movsim.simulator.vehicles.TrafficCompositionGenerator;
import org.movsim.simulator.vehicles.Vehicle;
import org.movsim.utilities.Units;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractTrafficSource implements SimulationTimeStep {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(AbstractTrafficSource.class);

    public interface RecordDataCallback {
        /**
         * Callback to allow the application to process or record the traffic source data.
         * 
         */
        public void recordData(double simulationTime, int laneEnter, double xEnter, double vEnter, double totalInflow,
                int enteringVehCounter, double nWait);
    }

    RecordDataCallback recordDataCallback;

    void recordData(double simulationTime, final double totalInflow) {
        if (recordDataCallback != null) {
            recordDataCallback.recordData(simulationTime, laneEnterLast, xEnterLast, vEnterLast, totalInflow,
                    enteringVehCounter, nWait);
        }
    }
    

    protected static final double MEASURING_INTERVAL_S = 60.0;
    private double measuredTime;
    private int measuredInflowCount;
    protected double measuredInflow;

    int enteringVehCounter;

    /** The x enter last. status of last merging vehicle for logging to file */
    double xEnterLast;

    double vEnterLast;

    int laneEnterLast;

    /** number of vehicles in the queue as result from integration over demand minus inserted vehicles. */
    double nWait;

    final TrafficCompositionGenerator vehGenerator;

    final RoadSegment roadSegment;
    
    Routing routing;
    ArrayList<Route> randomRoutes;
    
    Random rand;// NASTAVI SEED 

    public final ArrayList<RoadSegment> outFlowRoadSegments = new ArrayList<>();
    
    protected int vehicleCounter;

    public AbstractTrafficSource(@Nullable TrafficCompositionGenerator vehGenerator, RoadSegment roadSegment, Routing routing) {
        rand = new Random(ProjectMetaData.getInstance().getSeed());
        this.vehGenerator = vehGenerator;
        this.roadSegment = Preconditions.checkNotNull(roadSegment);
        this.routing = routing;
        nWait = 0;
        measuredInflow = 0;
        measuredTime = 0;
        measuredInflowCount = 0;
        vehicleCounter = 0;
        randomRoutes = new ArrayList<Route>();
       //Andraz DODANO ZA ODPRAVO TEST ERRORJEV
        if(this.routing != null){
            initOutflowRoadSegments(this.roadSegment,this.routing.outFlowRoadSegments);
            generateRandomRoutes();
        }
    }
     
    /**
     * Initialize all reachable outflowRoadSegments for this traffic source.
     * 
     * @param allOutflowRoadSegments
     */
    public final void initOutflowRoadSegments(RoadSegment roadSegment, ArrayList<RoadSegment> allOutflowRoadSegments){
        for(RoadSegment roadSeg : allOutflowRoadSegments){
            Route route = routing.findRoute(roadSegment, roadSeg);
            if (route != null) {
                outFlowRoadSegments.add(roadSeg);
            }
        }
        
    }
    
    public final void generateRandomRoutes(){
        //randomRoutes = new ArrayList<Route>();
        for(int i=0; i<outFlowRoadSegments.size();i++){
            Route route=routing.findRoute(roadSegment, outFlowRoadSegments.get(i));
            //while(route==null)
            //    route = routing.findRoute(roadSegment, outFlowRoadSegments.get(rand.nextInt(outFlowRoadSegments.size())));
            if (route != null)
                randomRoutes.add(route);
        }
        
    }
    /**
     * Sets the traffic source recorder.
     * 
     * @param recordDataCallback
     */
    public void setRecorder(RecordDataCallback recordDataCallback) {
        enteringVehCounter = 0;
        this.recordDataCallback = recordDataCallback;
    }
    /**
     * Sets seed of Random() object(rand). Needed when simulation is restarting, when we want to setup the same simulation.
     * 
     * @param seed
     */
    public void setSeed(int seed) {
        this.rand.setSeed(seed);
    }
    
    /**
     * Gets the entering veh counter.
     * 
     * @return the entering veh counter
     */
    public int getEnteringVehCounter() {
        return enteringVehCounter;
    }

    /**
     * Gets the total inflow over all lanes.
     * 
     * @param time
     *            the time
     * @return the total inflow over all lanes
     */
    public abstract double getTotalInflow(double time);

    /**
     * Gets the total number of generated vehicles
     * 
     * @return total number of generated vehicles.
     */
    public int getVehicleCounter() {
        return vehicleCounter;
    }
    /**
     * Returns the number of vehicles in the queue.
     * 
     * @return integer queue length over all lanes
     */
    public int getQueueLength() {
        return (int) nWait;
    }

    /**
     * Adds a the vehicle to the {@link LaneSegment} at initial front position with initial speed.
     */
    protected Vehicle addVehicle(LaneSegment laneSegment, TestVehicle testVehicle, double frontPosition, double speed) {
        final Vehicle vehicle = vehGenerator.createVehicle(testVehicle);
        initVehicle(laneSegment, frontPosition, speed, vehicle);
        //vehicle.physicalQuantities().getSpeed();
        vehicleCounter++;
        return vehicle;
    }

    protected void initVehicle(LaneSegment laneSegment, double frontPosition, double speed, final Vehicle vehicle) {
        vehicle.setFrontPosition(frontPosition);
        vehicle.setSpeed(speed);
        /*if(randomRoutes.size()<1)*/
            
        if (vehicle.lane() != laneSegment.lane()) {
            vehicle.setLane(laneSegment.lane());
        }
        
        
        if (vehicle.route == null) {

            //vehicle.route = routing.findRoute(roadSegment, outFlowRoadSegments.get(rand.nextInt(outFlowRoadSegments.size())));
            vehicle.route = randomRoutes.get(rand.nextInt(randomRoutes.size()));
        }
        vehicle.setRoadSegment(roadSegment.id(), roadSegment.roadLength());
//this.routing.findRoute(roadSegment, roadSegment)
        //System.out.println("Vehicle roadID" + vehicle.roadSegmentId());
        laneSegment.addVehicle(vehicle);
        // status variables of entering vehicle for logging
        enteringVehCounter++;
        xEnterLast = frontPosition;
        vEnterLast = speed;
        laneEnterLast = laneSegment.lane();
    }
    
    /**
     * Gets the new cyclic lane for entering.
     * 
     * @param iLane
     *            the i lane
     * @return the new cyclic lane for entering
     */
    protected int getNewCyclicLaneForEntering(int iLane) {
        return (iLane == roadSegment.laneCount()) ? Lanes.MOST_INNER_LANE : iLane + Lanes.TO_RIGHT;
    }

    /**
     * Returns the measured inflow in vehicles per second, averaged over the measuring interval.
     * 
     * @return measured inflow over all lanes in vehicles per seconds
     * 
     */
    public double measuredInflow() {
        return measuredInflow;
    }

    protected void incrementInflowCount(int incr) {
        measuredInflowCount += incr;
    }

    protected void calcApproximateInflow(double dt) {
        measuredTime += dt;
        if (measuredTime > MEASURING_INTERVAL_S) {
            measuredInflow = measuredInflowCount / MEASURING_INTERVAL_S; // vehicles per second
            measuredTime = 0.0;
            measuredInflowCount = 0;
            LOG.debug(String.format(
                    "source=%d with measured inflow Q=%.1f/h over all lanes and queue length %d of waiting vehicles",
                    roadSegment.id(), measuredInflow * Units.INVS_TO_INVH, getQueueLength()));
        }
    }
    public void reset(){
        measuredInflow = 0;
        measuredTime = 0;
        measuredInflowCount = 0;
        nWait = 0.0;
        vehicleCounter = 0; // not needed
        enteringVehCounter = 0;
        rand = new Random(ProjectMetaData.getInstance().getSeed());
        //generateRandomRoutes();
    }
}
