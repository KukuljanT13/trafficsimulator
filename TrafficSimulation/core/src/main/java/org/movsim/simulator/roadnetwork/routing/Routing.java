/*
 * Copyright (C) 2010, 2011, 2012 by Arne Kesting, Martin Treiber, Ralph Germ, Martin Budden
 * <movsim.org@gmail.com>
 * -----------------------------------------------------------------------------------------
 * 
 * This file is part of
 * 
 * MovSim - the multi-model open-source vehicular-traffic simulator.
 * 
 * MovSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MovSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MovSim. If not, see <http://www.gnu.org/licenses/>
 * or <http://www.movsim.org>.
 * 
 * -----------------------------------------------------------------------------------------
 */
package org.movsim.simulator.roadnetwork.routing;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;
import com.sun.prism.paint.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.AbstractGraph;
import org.movsim.autogen.Routes;
import org.movsim.simulator.roadnetwork.RoadNetwork;
import org.movsim.simulator.roadnetwork.RoadSegment;
import org.movsim.simulator.roadnetwork.RoadSegment.NodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Routing implements Cloneable{

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(Routing.class);

    private final Map<String, Route> predefinedRoutes;

    private final RoadNetwork roadNetwork;
    
    public final ArrayList<RoadSegment> outFlowRoadSegments = new ArrayList<>(); //end or outflow road segments

    // see http://jgrapht.org/ for library documentation
    private WeightedGraph<Long, RoadSegment> graph;
    
    private HashMap<String,Double> weightMatrix;

    public Routing(Routes routesInput, RoadNetwork roadNetwork) {
        this.roadNetwork = Preconditions.checkNotNull(roadNetwork);
        predefinedRoutes = Maps.newHashMap();
        if (routesInput != null) {
            createPredefinedRoutes(routesInput);
        }
        graph = NetworkGraph.create(roadNetwork);
        
        getAllSinkTrafficRoads();
    }

    private void createPredefinedRoutes(Routes routesInput) {
        for (org.movsim.autogen.Route routeInput : routesInput.getRoute()) {
            Route route = new Route(routeInput.getLabel());
            for (org.movsim.autogen.Road roadInput : routeInput.getRoad()) {
                RoadSegment roadSegment = roadNetwork.findByUserId(roadInput.getId());
                Preconditions.checkNotNull(roadSegment, "cannot create route \"" + route.getName()
                        + "\" with undefinied road=" + roadInput.getId());
                route.add(roadSegment);
            }
            Route replaced = predefinedRoutes.put(route.getName(), route);
            if (replaced != null) {
                throw new IllegalArgumentException("route with name=" + route.getName() + " already defined.");
            }
        }
        LOG.info("created " + predefinedRoutes.size() + " predefined routes.");
    }

    /**
     * Returns a list of all outflow roads(end roads). Also saves those roads to private class variable outFlowRoadSegments.
     * 
     * @return a list of all outflow roads(end roads).
     */
    public final ArrayList<RoadSegment> getAllSinkTrafficRoads(){
        if(outFlowRoadSegments.size() != 0)
            return outFlowRoadSegments;
        
        Iterator iterator = this.roadNetwork.iterator();
        while(iterator.hasNext()){
            RoadSegment road = (RoadSegment) iterator.next();
            if (road.sink() != null) {
                outFlowRoadSegments.add(road);
            }
        }
        return outFlowRoadSegments;
    }
    /**
     * Returns the {@link Route} for the route's name.
     * 
     * @param name
     * @return
     * @throws IllegalStateException
     */
    public Route get(String name) throws IllegalStateException {
        Preconditions.checkArgument(name != null && !name.isEmpty());
        Route route = predefinedRoutes.get(name);
        if (route == null) {
            throw new IllegalStateException("route with name \"" + name + "\" not defined.");
        }
        return route;
    }

    public boolean hasRoute(String name) {
        return predefinedRoutes.containsKey(name);
    }

    public Route findRoute(RoadSegment start, RoadSegment destination) throws IllegalStateException {
        return findRoute(start.userId(), destination.userId());
    }

    public Route findRoute(String startRoadId, String destinationRoadId) throws IllegalStateException {
        if (graph == null) {
            graph = NetworkGraph.create(roadNetwork);
        }
        Preconditions.checkArgument(startRoadId != null && !startRoadId.isEmpty());
        Preconditions.checkArgument(destinationRoadId != null && !destinationRoadId.isEmpty());

        Route route = new Route(createRouteName(startRoadId, destinationRoadId));
        RoadSegment startRoadSegment = roadNetwork.findByUserId(startRoadId);
        route.add(startRoadSegment);
        RoadSegment endRoadSegment = roadNetwork.findByUserId(destinationRoadId);

        
        LOG.info("Shortest path from roadSegment={} to={}", startRoadId, destinationRoadId);
        LOG.info("From node={} to node={}", startRoadSegment.getNode(NodeType.DESTINATION),
                endRoadSegment.getNode(NodeType.DESTINATION));

        List<RoadSegment> path = DijkstraShortestPath.findPathBetween(graph,
                startRoadSegment.getNode(NodeType.DESTINATION), endRoadSegment.getNode(NodeType.DESTINATION));
        
        if (path == null) {
            //throw new IllegalStateException("cannot find route from startRoadId=" + startRoadId + " to destinationRoadId=" + destinationRoadId);
            return null;
        }

        for (RoadSegment rs : path) {
            route.add(rs);
            LOG.info("roadSegment on path={}", rs);
        }
        return route;
    }

    /**
     * Method for applying map with road weights to graph.
     * 
     * @param weights
     * @return
     */
    public boolean setGraphWeights(HashMap<String,Double> weights){
        if (graph!=null) {
            for (RoadSegment roadSegment : roadNetwork) {
                graph.setEdgeWeight(roadSegment, weights.get(roadSegment.userId()));
            }
        }
        
        return true;
    }
    
    /**
     * Temporary for testing webservice
     * @param weights
     * @return 
     */
    public boolean setGraphWeights(ArrayList<Double> weights){
        if (graph!=null) {
            int index = 0;
            for (RoadSegment roadSegment : roadNetwork) {
                graph.setEdgeWeight(roadSegment, weights.get(index));
                index++;
            }
        }else{
            System.out.println("There is no graph generated!!! You can't apply weights!");
        }
        
        return true;
    }
    
    /**
     * Temporary for testing webservice
     * @return 
     */
    public int getGraphSize(){
        return roadNetwork.size();
    }
    /**
     * Loops through graph and generates HashMap with road weights.
     * 
     * @return  HashMap<String,Double>
     */
    public void setWeightMatrix(RoadSegment roadWorkRoad, double sliderValue){
        Map map = new HashMap();
        double max = -10;
        double maxLength = -1;
        for (RoadSegment roadSegment : roadNetwork) {
            double vehiclesPerLength = roadSegment.calculateVehiclePerLength();
            double roadLength = roadSegment.roadLength();
            if (vehiclesPerLength > max) {
                max = vehiclesPerLength;
            }
            if (roadLength > maxLength) {
                maxLength = roadLength;
            }

        }
        /*try {*/
           /* FileWriter fstream = new FileWriter("weightsOnEmptyRoad.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);*/
            
            for (RoadSegment roadSegment : roadNetwork) {
                double weight = ((roadSegment.calculateVehiclePerLength()/max)*sliderValue)+((roadSegment.roadLength()/maxLength)*(1-sliderValue));
                map.put(roadSegment.userId(), weight);
                roadSegment.setWeight(weight);
                if (roadSegment.equals(roadWorkRoad)){
                     
                    map.put(roadSegment.userId(), 2.0);
                    roadSegment.setWeight(2.0);
                }
                
                
                /*out.write(roadSegment.userId() + "\t" + (((roadSegment.calculateVehiclePerLength()/max)/2)+(roadSegment.roadLength()/maxLength)/2));
                out.newLine();*/
            }
                
                

            /*out.close();
            //logger.info("x: " + x); //$NON-NLS-1$
            //logger.info("y: " + y); //$NON-NLS-1$

        } catch (IOException ex) {
            System.out.println(ex);
        } */
        
        weightMatrix = (HashMap<String, Double>) map;
    }

    public HashMap<String, Double> getWeightMatrix() {
        return weightMatrix;
    }
    
    
    private static String createRouteName(String startRoadId, String destinationRoadId) {
        StringBuilder sb = new StringBuilder();
        sb.append("from_").append(startRoadId).append("_").append(destinationRoadId);
        return sb.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }
    

}
