/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.simulator.blockades;

import com.google.common.base.Preconditions;
import javax.swing.text.Position;
import org.movsim.simulator.roadnetwork.RoadSegment;
import org.movsim.simulator.trafficlights.TrafficLight;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Andraz
 */
public class Blockade {
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(TrafficLight.class);

    /** The status. */
    private boolean status=false;
    
    private double position = Double.NaN;
    
    private RoadSegment roadSegment;
    
    private boolean alreadySetByAnotherCriticalInterval = false;
    
    public Blockade(RoadSegment roadSegment, double position){
        this.roadSegment = roadSegment;
        this.position = position;
    }
    public double position() {
        Preconditions.checkArgument(!Double.isNaN(position), "blockade without position");
        return position;
    }

    public boolean hasPosition() {
        return !Double.isNaN(position);
    }

    public void setPosition(double position) {
        Preconditions.checkArgument(Double.isNaN(this.position), "position already set: " + toString());
        this.position = position;
    }
    
    public boolean status() {
        return status;
    }

    public void setState(boolean newStatus) {
        this.status = newStatus;
    }
    
    public RoadSegment roadSegment() {
        return Preconditions.checkNotNull(roadSegment);
    }

    public void setRoadSegment(RoadSegment roadSegment) {
        Preconditions.checkArgument(this.roadSegment == null, "roadSegment already set");
        this.roadSegment = roadSegment;
    }

    public boolean isAlreadySetByAnotherCriticalInterval() {
        return alreadySetByAnotherCriticalInterval;
    }

    public void setAlreadySetByAnotherCriticalInterval(boolean isAlreadySetByAnotherCriticalInterval) {
        this.alreadySetByAnotherCriticalInterval = isAlreadySetByAnotherCriticalInterval;
    }

    
    @Override
    public String toString() {
        return "Blockade [status=" + status + ", position=" + position 
                + ", roadSegment.id=" + ((roadSegment == null) ? "null" : roadSegment.id()) + "]";
    }
    
    public void reset(){
        status = false;
        alreadySetByAnotherCriticalInterval = false;
        
    }
}
