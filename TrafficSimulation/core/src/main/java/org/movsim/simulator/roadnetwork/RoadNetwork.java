/*
 * Copyright (C) 2010, 2011, 2012 by Arne Kesting, Martin Treiber, Ralph Germ, Martin Budden
 * <movsim.org@gmail.com>
 * -----------------------------------------------------------------------------------------
 * 
 * This file is part of
 * 
 * MovSim - the multi-model open-source vehicular-traffic simulator.
 * 
 * MovSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MovSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MovSim. If not, see <http://www.gnu.org/licenses/>
 * or <http://www.movsim.org>.
 * 
 * -----------------------------------------------------------------------------------------
 */

package org.movsim.simulator.roadnetwork;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.movsim.input.ProjectMetaData;
import org.movsim.simulator.SimulationTimeStep;
import org.movsim.simulator.Simulator;
import org.movsim.simulator.blockades.BlockadeLocation;
import org.movsim.simulator.blockades.CriticalInterval;
import org.movsim.simulator.roadnetwork.JunctionSetOfRoads;
import org.movsim.simulator.roadnetwork.routing.Route;
import org.movsim.simulator.vehicles.Vehicle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Iterable collection of the road segments in the road network.
 */
public class RoadNetwork implements SimulationTimeStep, Iterable<RoadSegment> {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(RoadNetwork.class);

    private final ArrayList<RoadSegment> roadSegments = new ArrayList<>();
    private final ArrayList<JunctionSetOfRoads> junctions = new ArrayList<>();

    Map<Integer, TrafficSink> trafficSinksMap;
    
    private String name;

    private boolean isWithCrashExit;
    private boolean hasVariableMessageSign;

    public Simulator simulator;

    public RoadNetwork(Simulator sim) {
        this.simulator = sim;
        this.trafficSinksMap = trafficSinksMap = new HashMap<Integer, TrafficSink>();
    }
    
    public void initializeTrafficSinks(){
        for (RoadSegment roadSegment : roadSegments) {
            if (roadSegment.sink() != null) {
                trafficSinksMap.put(Integer.parseInt(roadSegment.userId()), roadSegment.sink());
            }
        }
    }
    public void initializeJunctionsCriticals(){
        for (JunctionSetOfRoads junc : junctions) {
            junc.setCriticalIntervals();
        }
    }
    /**
     * Returns the array of junctions
     * 
     * @return the array of junctions.
     */
     public ArrayList<JunctionSetOfRoads> getJunctions() {
        return junctions;
    }
     
     public RoadSegment getRoadOnIndex(int index){
         return roadSegments.get(index);
     }
    /**
     * Sets the name of the road network.
     * 
     * @param name
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of the road network.
     * 
     * @return the name of the road network
     */
    public final String name() {
        return name;
    }

    /**
     * Given its id, find a road segment in the road network.
     * 
     * @param id
     * @return the road segment with the given id
     */
    public RoadSegment findById(int id) {
        for (final RoadSegment roadSegment : roadSegments) {
            if (roadSegment.id() == id) {
                return roadSegment;
            }
        }
        return null;
    }

    public void setTrafficLightsIntervalsToJunctions(ArrayList<int[]> intervals){
        if(intervals.size() != junctions.size()){
            System.out.println("Size of junctions do not match with interval array size!!!");
            return;
        }
        for(int i=0; i<intervals.size(); i++){
            junctions.get(i).setStartTime(intervals.get(i)[0]);
            junctions.get(i).setTrafficLightIntervals(intervals.get(i)[1], intervals.get(i)[2]);
        }
        
    }
    /**
     * Given its userId, find a road segment in the road network.
     * 
     * @param userId
     * @return the road segment with the given userId
     */
    public RoadSegment findByUserId(String userId) {
        for (final RoadSegment roadSegment : roadSegments) {
            if (roadSegment.userId() != null && roadSegment.userId().equals(userId)) {
                return roadSegment;
            }
        }
        return null;
    }

    /**
     * Clear the road network so that it is empty and ready to accept new RoadSegments, Vehicles, sources, sinks and
     * junctions.
     */
    public void clear() {
        name = null;
        hasVariableMessageSign = false;
        // LaneChangeModel.resetCount();
        // LongitudinalDriverModel.resetNextId();
        RoadSegment.resetNextId();
        // TrafficFlowBase.resetNextId();
        // Vehicle.resetNextId();
        roadSegments.clear();
        junctions.clear();
    }
    public void removeAllVehicles(){
        for (RoadSegment roadSegment : this) {
            roadSegment.reset();
        }
    }
    /**
     * Called when the system is running low on memory, and would like actively running process to try to tighten their
     * belts.
     */
    public void onLowMemory() {
        roadSegments.trimToSize();
    }

    /**
     * Returns the number of RoadSegments in the road network.
     * 
     * @return the number of RoadSegments in the road network
     */
    public final int size() {
        return roadSegments.size();
    }

    /**
     * Adds a road segment to the road network.
     * 
     * @param roadSegment
     * @return roadSegment for convenience
     */
    public RoadSegment add(RoadSegment roadSegment)  {
        assert roadSegment != null;
        assert roadSegment.eachLaneIsSorted();
        roadSegments.add(roadSegment);
        //*temporary helper to collect all in/from junction roads*//
        /*if(Integer.parseInt(roadSegment.userId())%20<8){
            String myTempString = roadSegment.userId() + "\t" + roadSegment.roadMapping().startPos().x + "\t" + roadSegment.roadMapping().startPos().y;
            try {
                FileWriter fstream = new FileWriter("roadsPositions.txt",true);
                BufferedWriter out = new BufferedWriter(fstream);


                out.write(myTempString);
                out.newLine();
                out.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }*/
        //*END*//
        return roadSegment;
    }
    

    /**
     * Adds a junction to the road network in junctions array list.
     * 
     * @param junction
     * @return junction for convenience
     */
    public JunctionSetOfRoads add(JunctionSetOfRoads junction)  {
        assert junction != null;
        junctions.add(junction);
        return junction;
    }
    /**
     * Returns a junction which include specified road segment.
     * 
     * @param rs
     * @return junction which include specified road segment.
     */
    public JunctionSetOfRoads getJunctionOfRoad(RoadSegment rs){
        int roadId = rs.id();
        for(JunctionSetOfRoads junction : junctions){
            for(RoadSegment road : junction.allRoads()){
                if (road.id() == roadId) {
                    return junction;
                }
            }
        }
        return null;
    }
    /**
     * Returns an iterator over all the road segments in the road network.
     * 
     * @return an iterator over all the road segments in the road network
     */
    @Override
    public Iterator<RoadSegment> iterator() {
        return roadSegments.iterator();
    }

    public ArrayList<RoadSegment> getRoadSegments(){
        return roadSegments;
    }
    /**
     * <p>
     * The main timestep of the simulation. Update of calculation of vehicle accelerations, movements, lane-changing decisions. Each update
     * step is applied in parallel to all vehicles <i>of the entire network</i>. Otherwise, inconsistencies would occur. In particular, the
     * complete old state (positions, lanes, speeds ...) is made available during the complete update step of one timestep. Then the outflow
     * is performed for each road segment, moving vehicles onto the next road segment (or removing them entirely from the road network) when
     * required. Then the inflow is performed for each road segment, adding any new vehicles supplied by any traffic sources. Finally the
     * vehicle detectors are updated.
     * </p>
     * 
     * <p>
     * The steps themselves are grouped into two main blocks and an auxillary block:
     * <ol type="a">
     * <li>Longitudinal update:</li>
     * <ol type="i">
     * <li>Calculate accelerations</li>
     * <li>update speeds
     * <li>update positions
     * </ol>
     * <li>Discrete Decision update:</li>
     * <ol type="i">
     * <li>Determine decisions (whether to change lanes, decide to cruise/stop at a traffic light, etc.)</li>
     * <li>perform decisions (do the lane changes, cruising/stopping at traffic light, etc.)</li>
     * </ol>
     * 
     * <li>Do the related bookkeeping (update of inflow and outflow at boundaries) and update virtual detectors</li>
     * </ol>
     * </p>
     * 
     * <p>
     * The blocks can be swapped as long as each block is done serially for the whole network in exactly the above order (i),(ii),(iii).
     * </p>
     * 
     * @param dt
     *            simulation time interval, seconds.
     * @param simulationTime
     *            the current logical time in the simulation
     * @param iterationCount
     *            the counter of performed update steps
     */
    @Override
    public void timeStep(double dt, double simulationTime, long iterationCount) {
        // Make each type of update for each road segment, this avoids problems with vehicles
        // being updated twice (for example when a vehicle moves of the end of a road segment
        // onto the next road segment.

        LOG.debug("called timeStep: time={}, timestep=", simulationTime, dt);
        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.updateRoadConditions(dt, simulationTime, iterationCount);
        }
            
        // Note: must do lane changes before vehicle positions are updated (or after outFlow) to ensure
        // the vehicle's roadSegmentId is correctly set
        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.makeLaneChanges(dt, simulationTime, iterationCount);
        }

        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.updateVehicleAccelerations(dt, simulationTime, iterationCount);
        }

        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.updateVehiclePositionsAndSpeeds(dt, simulationTime, iterationCount);
        }

        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.checkForInconsistencies(simulationTime, iterationCount, isWithCrashExit);
        }

        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.outFlow(dt, simulationTime, iterationCount);
        }

        for (final RoadSegment roadSegment : roadSegments) {
            roadSegment.inFlow(dt, simulationTime, iterationCount);
        }

        for (final RoadSegment roadSegment : roadSegments) {
           /* if(roadSegment.userId().equals("4")){
                System.out.println("1234");
                for (Vehicle vehicle : roadSegment.laneSegments[1].vehicles) {
                    System.out.println(vehicle.toString());
                }
                System.out.println("\n");
            }*/
            roadSegment.updateDetectors(dt, simulationTime, iterationCount);
        }
        
        resetBlockades();//Set variable alreadySetByAnotherCrititcalInterval to false on all lanesegments of all roadsegments.
        for(final JunctionSetOfRoads junction : junctions) {
            junction.updateBlockades(dt,simulationTime, iterationCount);
        }
        
        /*if(this.simulator.result >199 )
            System.out.println("eorgjijiiojeriojs");*/
        this.simulator.setStoppedVehiclesTime(this.simulator.getStoppedVehiclesTime() + (dt*getStoppedVehicleCount()));

    }
    public void setWithCrashExit(boolean isWithCrashExit) {
        this.isWithCrashExit = isWithCrashExit;
    }

    public void setHasVariableMessageSign(boolean hasVariableMessageSign) {
        this.hasVariableMessageSign = hasVariableMessageSign;
    }

    public boolean hasVariableMessageSign() {
        return hasVariableMessageSign;
    }

    /**
     * Returns the number of vehicles on this road network.
     * 
     * @return the number of vehicles on this road network
     */
    public int vehicleCount() {
        int vehicleCount = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            vehicleCount += roadSegment.getVehicleCount();
        }
        return vehicleCount;
    }
    


    public int getStoppedVehicleCount() {
        int stoppedVehicleCount = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            stoppedVehicleCount += roadSegment.getStoppedVehicleCount();
        }
        return stoppedVehicleCount;
    }
    public int getNumberOfCrashedVehicles(){
        int counter = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            counter += roadSegment.getNumberOfCrashedVehicles();
        }
        return counter;
    }
    public int getNumberOfLostVehicles(){
        int counter = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            counter += roadSegment.getNumberOfLostVehicles();
        }
        return counter;
    }
    public int getVehiclesRemovedBySinks(){
        int counter = 0;
        Iterator it = trafficSinksMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            TrafficSink trafficSink = (TrafficSink) pairs.getValue();
            counter += trafficSink.getTotalVehiclesRemoved();
        }
        return counter;
    }

    public double vehiclesMeanSpeed() {
        double averageSpeed = 0;
        int count = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            double meanSpeed = roadSegment.meanSpeed();
            if (meanSpeed > 0.)
            averageSpeed += meanSpeed;
            else 
                count++;
        }
        return averageSpeed / (roadSegments.size() - count);
    }

    /**
     * Returns the number of obstacles on this road network.
     * 
     * @return the number of obstacles on this road network
     */
    public int obstacleCount() {
        int obstacleCount = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            obstacleCount += roadSegment.obstacleCount();
        }
        return obstacleCount;
    }

    /**
     * Returns the number of obstacles for the given route.
     * 
     * @return the number of obstacles on the given route
     */
    public int obstacleCount(Route route) {
        int obstacleCount = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            obstacleCount += roadSegment.obstacleCount();
        }
        return obstacleCount;
    }
    /*/**
     * Returns the number of obstacles on this road network.
     * 
     * @return the number of obstacles on this road network
     */
    /*public int obstacleCount() {
        int obstacleCount = 0;
        for (final RoadSegment roadSegment : roadSegments) {
            obstacleCount += roadSegment.getObstacleCount();
        }
        return obstacleCount;
    }*/
    /**
     * Asserts the road network's class invariant. Used for debugging.
     */
    public boolean assertInvariant() {
        for (final RoadSegment roadSegment : roadSegments) {
            assert roadSegment.assertInvariant();
        }
        return true;
    }

    /**
     * Returns the number of vehicles on route.
     * 
     * @return the number of vehicles on given route.
     */
    public static int vehicleCount(Route route) {
        int vehicleCount = 0;
        for (final RoadSegment roadSegment : route) {
            vehicleCount += roadSegment.getVehicleCount();
        }
        return vehicleCount;
    }

    /**
     * Returns the total travel time of all vehicles on this road network, including those that have exited.
     * 
     * @return the total vehicle travel time
     */
    public double totalVehicleTravelTime() {
        double totalVehicleTravelTime = 0.0;
        for (RoadSegment roadSegment : roadSegments) {
            totalVehicleTravelTime += roadSegment.totalVehicleTravelTime();
            if (roadSegment.sink() != null) {
                totalVehicleTravelTime += roadSegment.sink().totalVehicleTravelTime();
            }
        }
        return totalVehicleTravelTime;
    }

    public static double totalVehicleTravelTime(Route route) {
        double totalVehicleTravelTime = 0.0;
        for (final RoadSegment roadSegment : route) {
            totalVehicleTravelTime += roadSegment.totalVehicleTravelTime();
            if (roadSegment.sink() != null) {
                totalVehicleTravelTime += roadSegment.sink().totalVehicleTravelTime();
            }
        }
        return totalVehicleTravelTime;
    }

    public static double instantaneousTravelTime(Route route) {
        double instantaneousTravelTime = 0;
        for (final RoadSegment roadSegment : route) {
            instantaneousTravelTime += roadSegment.instantaneousTravelTime();
        }
        return instantaneousTravelTime;
    }

    /**
     * Returns the total travel distance of all vehicles on this road network, including those that have exited.
     * 
     * @return the total vehicle travel distance
     */
    public double totalVehicleTravelDistance() {
        double totalVehicleTravelDistance = 0.0;
        for (RoadSegment roadSegment : roadSegments) {
            double x=roadSegment.totalVehicleTravelDistance();
            totalVehicleTravelDistance += x;
            if (roadSegment.sink() != null) {
                totalVehicleTravelDistance += roadSegment.sink().totalVehicleTravelDistance();
            }
        }
        return totalVehicleTravelDistance;
    }

    public static double totalVehicleTravelDistance(Route route) {
        double totalVehicleTravelDistance = 0.0;
        for (final RoadSegment roadSegment : route) {
            totalVehicleTravelDistance += roadSegment.totalVehicleTravelDistance();
            if (roadSegment.sink() != null) {
                totalVehicleTravelDistance += roadSegment.sink().totalVehicleTravelDistance();
            }
        }
        return totalVehicleTravelDistance;
    }
    
    /**
     * Returns the average speed of all vehicles on this road network, including those that have exited.
     * 
     * @return the total vehicle travel distance
     */
    public double averageVehicleSpeed() {
        double allAverageVehicleSpeed = 0.0;
        double vehicleCount = 0;
        for (RoadSegment roadSegment : roadSegments) {
            allAverageVehicleSpeed += roadSegment.sumAverageVehicleSpeed();
            vehicleCount += roadSegment.getVehicleCount();
            if (roadSegment.sink() != null) {
                allAverageVehicleSpeed += roadSegment.sink().sumAverageVehicleSpeed();
                vehicleCount += roadSegment.sink().getTotalVehiclesRemoved();
            }
        }
        //vehicleCount += getVehiclesRemovedBySinks();
        
        return (allAverageVehicleSpeed/vehicleCount);
    }
    
    /**
     * Returns the relative average not-moving vehicles time on their roads positioned on this road network, including those that have exited.
     * 
     * @return the total vehicle travel distance
     */
    public double averageStoppedVehicleTime() {
        double allAverageStoppedVehicleTime = 0.0;
        double vehicleCount = 0;
        for (RoadSegment roadSegment : roadSegments) {
            allAverageStoppedVehicleTime += roadSegment.sumAveragestoppedVehicleTime();
            vehicleCount += roadSegment.getVehicleCount();
            if (roadSegment.sink() != null) {
                allAverageStoppedVehicleTime += roadSegment.sink().totalAverageStoppedVehicleTravelTime();
                vehicleCount += roadSegment.sink().getTotalVehiclesRemoved();
            }
        }
        //vehicleCount += getVehiclesRemovedBySinks();
        
        return (allAverageStoppedVehicleTime/vehicleCount);
    }
    
    /**
     * Returns the relative average acceleration time on this road network, including those that have exited.
     * 
     * @return the total vehicle travel distance
     */
    public double averagePosAccVehicleTime() {
        double allAveragePosAccVehicleTime = 0.0;
        double vehicleCount = 0;
        for (RoadSegment roadSegment : roadSegments) {
            allAveragePosAccVehicleTime += roadSegment.sumAveragePosAccTime();
            vehicleCount += roadSegment.getVehicleCount();
            if (roadSegment.sink() != null) {
                allAveragePosAccVehicleTime += roadSegment.sink().totalAverageStoppedVehicleTravelTime();
                vehicleCount += roadSegment.sink().getTotalVehiclesRemoved();
            }
        }
        //vehicleCount += getVehiclesRemovedBySinks();
        
        return (allAveragePosAccVehicleTime/vehicleCount);
    }

    /**
     * Returns the total fuel used by all vehicles on this road network, including those that have exited.
     * 
     * @return the total vehicle fuel used
     */
    public double totalVehicleFuelUsedLiters() {
        double totalVehicleFuelUsedLiters = 0.0;
        for (RoadSegment roadSegment : roadSegments) {
            totalVehicleFuelUsedLiters += roadSegment.totalVehicleFuelUsedLiters();
            if (roadSegment.sink() != null) {
                totalVehicleFuelUsedLiters += roadSegment.sink().totalFuelUsedLiters();
            }
        }
        return totalVehicleFuelUsedLiters;
    }

    public static double instantaneousFuelUsedLiters(Route route) {
        double instantaneousConsumption = 0;
        for (final RoadSegment roadSegment : route) {
            instantaneousConsumption += roadSegment.instantaneousConsumptionLitersPerSecond();
        }
        return instantaneousConsumption;
    }

    private void resetBlockades(){
        for(final RoadSegment roadSegment : roadSegments) {
            for (final LaneSegment laneSegment : roadSegment.laneSegments()) {
                CriticalInterval criticalInterval = laneSegment.getCriticalInterval();
                BlockadeLocation blockadeLoc = roadSegment.getNextDownstreamBlockade(0);
                if(blockadeLoc == null)
                    continue;
                
                blockadeLoc.getBlockade().setAlreadySetByAnotherCriticalInterval(false);
            }
        }
     
     }
    public void reset(){
        removeAllVehicles();

        Iterator it = trafficSinksMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            TrafficSink trafficSink = (TrafficSink) pairs.getValue();
            trafficSink.reset();
        }
        for (JunctionSetOfRoads junction : junctions ) {
            junction.reset();
        }
        
    }
}
