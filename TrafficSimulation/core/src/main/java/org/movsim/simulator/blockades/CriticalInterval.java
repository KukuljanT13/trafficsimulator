/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.simulator.blockades;

import org.movsim.simulator.roadnetwork.RoadSegment;
import org.movsim.simulator.trafficlights.TrafficLight;

/**
 *
 * @author Andraz
 */
public class CriticalInterval {
    
    
    private RoadSegment roadSegment;
    private double startPosition;
    private double endPosition;
    private double minSpeed; //minimum speed to set a blockade


    RoadSegment blockadeRoadSegment; //This is the road where a blockade has to be set.
    private double blockadePosition; //position of blockade on roadSegment
    
    TrafficLight watchForTrafficLight;
    
    /*public CriticalInterval(){
        startPosition = 0.0;
        endPosition = 0.0;
        roadSegment = null;
    }
    public CriticalInterval(double startPosition, double endPosition){
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }*/
    public CriticalInterval(RoadSegment roadSegment,double startPosition, double endPosition, RoadSegment blockadeRoadSegment, double blockadePosition){
        assert blockadeRoadSegment.roadLength() < blockadePosition;
        this.roadSegment = roadSegment;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.blockadeRoadSegment = blockadeRoadSegment;
        this.blockadePosition = blockadePosition;
        getOrCreateBlockade(); //To initialize and add BlockadeLocation to roadsegment
    }

    public TrafficLight getWatchTrafficLight() {
        return watchForTrafficLight;
    }
    
    public double getStartPosition() {
        return startPosition;
    }

    public RoadSegment getRoadSegment() {
        return roadSegment;
    }
    
    public RoadSegment getBlockadeRoadSegment() {
        return blockadeRoadSegment;
    }

    public double getBlockadePosition() {
        return blockadePosition;
    }

    
    public double getEndPosition() {
        return endPosition;
    }
    
    public double getMinSpeed() {
        return minSpeed;
    }

    public void setMinSpeed(double minSpeed) {
        this.minSpeed = minSpeed;
    }
    
    public void setWatchTrafficLight(TrafficLight watchTrafficLight) {
        this.watchForTrafficLight = watchTrafficLight;
    }
    
    public void setBlockade(){
        BlockadeLocation blockadeLoc = getOrCreateBlockade();
        blockadeLoc.getBlockade().setAlreadySetByAnotherCriticalInterval(true);
        blockadeLoc.getBlockade().setState(true);
    }
    
    public void releaseBlockade(){
        BlockadeLocation blockadeLoc = getOrCreateBlockade();
        if (blockadeLoc.getBlockade().isAlreadySetByAnotherCriticalInterval()) {
            return;
        }
        blockadeLoc.getBlockade().setState(false);
    }
    
    private BlockadeLocation getOrCreateBlockade(){
        BlockadeLocation blockadeLoc = blockadeRoadSegment.getBlockadeOnPosition(blockadePosition);
        Blockade blockade;
        if (blockadeLoc == null) {
            blockade = new Blockade(blockadeRoadSegment,blockadePosition);
            blockadeLoc = new BlockadeLocation(blockadePosition,blockade);
            blockadeRoadSegment.addBlockadeLocation(blockadeLoc);
        }
        return blockadeLoc;   
    }
    
}
