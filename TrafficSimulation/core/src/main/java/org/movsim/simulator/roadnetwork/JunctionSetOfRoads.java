/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.movsim.simulator.roadnetwork;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.movsim.autogen.Phase;
import org.movsim.autogen.TrafficLightStatus;
import org.movsim.simulator.blockades.Blockade;
import org.movsim.simulator.blockades.BlockadeLocation;
import org.movsim.simulator.blockades.CriticalInterval;
import org.movsim.simulator.trafficlights.TrafficLight;
import org.movsim.simulator.trafficlights.TrafficLightControlGroup;
import org.movsim.simulator.vehicles.Vehicle;
import org.movsim.simulator.vehicles.Vehicle.Type;

/**
 *
 * @author Andraz
 */
public class JunctionSetOfRoads {
   
    //ArrayList<RoadSegment> roads = new ArrayList<>();
    TrafficLightControlGroup trafficLightController;
    public int startTime=0;
    private int junctionID;
    
    //InsideRoads
    ArrayList<RoadSegment> roadsTurningLeft = new ArrayList<>();
    ArrayList<RoadSegment> roadsTurningRight = new ArrayList<>();
    ArrayList<RoadSegment> roadsStreight = new ArrayList<>();
    
    //in/out roads
    ArrayList<RoadSegment> roadsIn = new ArrayList<>();
    ArrayList<RoadSegment> roadsOut = new ArrayList<>();
    
    ArrayList<CriticalInterval> criticals = new ArrayList<>();
    
    boolean hasToWaitForEmptyJunction=false;
   
    public JunctionSetOfRoads(ArrayList<RoadSegment> roads){
        initializeRoads(roads);
        this.junctionID = Integer.parseInt(roads.get(0).userId()) / 20;
        hasToWaitForEmptyJunction = true;
    } 
    
    public JunctionSetOfRoads(ArrayList<RoadSegment> roadsIn, ArrayList<RoadSegment> roadsOut, int junctionID){
        this.roadsIn.addAll(roadsIn);
        this.roadsOut.addAll(roadsOut);
        this.junctionID = junctionID;
        hasToWaitForEmptyJunction = true;
    }

    public void initializeRoads(ArrayList<RoadSegment> roads){
        for (RoadSegment roadSegment : roads) {
            switch(Integer.parseInt(roadSegment.userId())%20){
                case 0:case 1:case 2:case 3:
                    roadsIn.add(roadSegment);
                    break;
                case 4:case 5:case 6:case 7:
                    roadsOut.add(roadSegment);
                    break;
                case 8:case 11:case 14:case 17:
                    roadsStreight.add(roadSegment);
                    break;
                case 9:case 12:case 15:case 18:
                    roadsTurningLeft.add(roadSegment);
                    break;
                case 10:case 13:case 16:case 19:
                    roadsTurningRight.add(roadSegment);
                    break;
            }   
        }

    }
    
    public ArrayList<RoadSegment> allRoads(){
        ArrayList<RoadSegment> roads = new ArrayList<>();
        roads.addAll(roadsIn);
        roads.addAll(roadsOut);
        roads.addAll(roadsStreight);
        roads.addAll(roadsTurningLeft);
        roads.addAll(roadsTurningRight);
        return roads;
    }
    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
        if(trafficLightController!=null)
            trafficLightController.setCurrentPhaseDuration(-startTime);
    }   
    
    public TrafficLightControlGroup getTrafficLightController() {
        return trafficLightController;
    }

    public void setTrafficLightController(TrafficLightControlGroup trafficLightController) {
        trafficLightController.setCurrentPhaseDuration(-startTime);
        /*trafficLightController.getTrafficLights().get("first").setRoadSegment(roads.get(0));
        trafficLightController.getTrafficLights().get("second").setRoadSegment(roads.get(2));
        trafficLightController.getTrafficLights().get("third").setRoadSegment(roads.get(1));
        trafficLightController.getTrafficLights().get("fourth").setRoadSegment(roads.get(3));*/
        this.trafficLightController = trafficLightController;
        
    }
    
    public void setTrafficLightIntervals(int interval1, int interval2){
        if (trafficLightController == null) {
            System.out.println("ERROR: There is no trafficLightControler set.");
            return;
        }
        List<Phase> phases = trafficLightController.getPhases();
        phases.get(0).setDuration(interval1);
        phases.get(4).setDuration(interval2);
        
    }
    
    public void setCriticalIntervals(){
        for (RoadSegment roadSegment : allRoads()) {
            //boolean blocadeNeeded = roadSegment.isVehicleOnInterval(5, 30);
            //RoadSegment rs = getSinkRoadForTurningLeft(getOnComingRoad(roadSegment));
            //BlockadeLocation blockadeLoc = rs.getNextDownstreamBlockade(0);
            int index = Integer.parseInt(roadSegment.userId())%20;
            if (index < 4) {//intervals hardcoded just for testing: ANDRAZ 
                /*Vehicle obstacle = new Vehicle(rs.roadLength()/2, 0.0, Lanes.LANE1, 1.0, 1.0);
                obstacle.setType(Vehicle.Type.OBSTACLE);
                rs.addObstacle(obstacle);*/
                
                
                RoadSegment rs = getSinkRoadForTurningLeft(getOnComingRoad(roadSegment));
                CriticalInterval ci = new CriticalInterval(roadSegment, 0, 30, rs, rs.roadLength()/2 );
                for (LaneSegment laneSegment : roadSegment.laneSegments()) {
                    laneSegment.setCriticalInterval(ci);
                }
                TrafficLight tf = getTrafficLight(ci.getRoadSegment());
                ci.setWatchTrafficLight(tf);
                criticals.add(ci);

                /*RoadSegment source = roadSegment.sourceRoadSegment(1);
                if(source == null)
                    System.out.println("wstjiasergjiserg");
                ci = new CriticalInterval(source, source.roadLength()-10, source.roadLength(), rs, rs.roadLength()/2 );
                for (LaneSegment laneSegment : roadSegment.laneSegments()) {
                    laneSegment.setCriticalInterval(ci);
                }
                ci.setWatchTrafficLight(tf);
                criticals.add(ci);*/
                
                RoadSegment criticalIntervaleRoad = getSinkRoadForTurningRight(roadSegment);
                ci = new CriticalInterval(criticalIntervaleRoad, 0, criticalIntervaleRoad.roadLength(), rs, rs.roadLength()/2 );
                for (LaneSegment laneSegment : criticalIntervaleRoad.laneSegments()) {
                    laneSegment.setCriticalInterval(ci);
                }
                criticals.add(ci);
                
                criticalIntervaleRoad = getSinkRoadStreight(roadSegment);
                ci = new CriticalInterval(criticalIntervaleRoad, 0, criticalIntervaleRoad.roadLength(), rs, rs.roadLength()/2 );
                for (LaneSegment laneSegment : criticalIntervaleRoad.laneSegments()) {
                    laneSegment.setCriticalInterval(ci);
                }
                criticals.add(ci);
                
                /*criticalIntervaleRoad = roadsTurningLeft.get(index);
                RoadSegment blockadeRoad = getSinkRoadForTurningRight(getOnComingRoad(roadsIn.get(index)));
                ci = new CriticalInterval(criticalIntervaleRoad, criticalIntervaleRoad.roadLength()/2,criticalIntervaleRoad.roadLength(),blockadeRoad, 10);
                for (LaneSegment laneSegment : criticalIntervaleRoad.laneSegments()) {
                    laneSegment.setCriticalInterval(ci);
                }
                criticals.add(ci);*/
               
                /*criticalIntervaleRoad = roadsOut.get((index+4)%4);
                ci = new CriticalInterval(criticalIntervaleRoad, 0, 9, roadSegment, roadSegment.roadLength());
                for (LaneSegment laneSegment : criticalIntervaleRoad.laneSegments()) {
                    laneSegment.setCriticalInterval(ci);
                }
                criticals.add(ci);*/
            }
            
            
        }
        for (RoadSegment roadSegment : roadsIn) {
             /*blockades for emtpyjunction*/
            BlockadeLocation blockadeLoc = new BlockadeLocation(roadSegment.roadLength(), new Blockade(roadSegment, roadSegment.roadLength()));
            roadSegment.addBlockadeLocation(blockadeLoc);
        }
    }
    public TrafficLight getTrafficLight(RoadSegment rs){
        Iterator it = trafficLightController.getTrafficLights().entrySet().iterator();
        TrafficLight tf = null;
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            TrafficLight trafficl = (TrafficLight)pairs.getValue();
            if(trafficl.roadSegment().userId().equals(rs.userId())){
                tf = trafficl;
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        return tf;
    }
       /**
     * Updates the blockades, if there are any.
     * 
     * @param dt
     *            delta-t, simulation time interval, seconds
     * @param simulationTime
     *            current simulation time, seconds
     * @param iterationCount
     *            the number of iterations that have been executed
     */
    public void updateBlockades(double dt, double simulationTime, long iterationCount){
         
        for(CriticalInterval critical : criticals){
            boolean blockadeNeeded = false;
            for(LaneSegment lane : critical.getRoadSegment().laneSegments()){
                for (final Vehicle vehicle : lane.vehicles) {
                    double vehiclePosition = vehicle.getFrontPosition();
                    double vehiclePositionBack = vehicle.getRearPosition();
                    if(simulationTime > 0 && (critical.getRoadSegment().userId().equals("9") /*|| 
                            critical.getRoadSegment().userId().equals("3") ||
                            critical.getRoadSegment().userId().equals("15")  ||
                            critical.getRoadSegment().userId().equals("18")*/) && vehiclePosition >= critical.getStartPosition() && vehiclePositionBack <= critical.getEndPosition())
                        System.out.println("25454356546");
                    if(vehiclePosition >= critical.getStartPosition() && vehiclePositionBack <= critical.getEndPosition()
                            && vehicle.getSpeed() > 1){
                        blockadeNeeded = true;
                        break;
                    }   
                    
                }
            }
           // if (critical.getWatchTrafficLight() != null) {//Integer.parseInt(critical.getRoadSegment().userId())%20 < 4 && critical.getRoadSegment().isIsJunctionRoad()
           //     TrafficLight light = critical.getWatchTrafficLight();
            //    TrafficLightStatus tfStatus = light.status();
            //    if ((tfStatus == TrafficLightStatus.RED && roadsDependingOnLeftTurnAreEmpty(critical.getBlockadeRoadSegment())/*tfStatus == TrafficLightStatus.GREEN_RED ||*/) ) {
           //         blockadeNeeded = false;
            //    } else if( tfStatus == TrafficLightStatus.GREEN && checkIfDeadLock(critical.getBlockadeRoadSegment())){
            //        blockadeNeeded = false;
            //    }
           // }
            //blockadeNeeded = isDeadLockWhenTurningLeft(critical.getBlockadeRoadSegment()) && blockadeNeeded;
            
            /*if (roadsIn.indexOf(critical.getRoadSegment()) != -1 && critical.getRoadSegment().getNumberOfVehiclesPastEnd() != 0) {
                blockadeNeeded = 
            }*/
            if (blockadeNeeded)
                critical.setBlockade();
            else
                critical.releaseBlockade();
                
        }
        
        updateBlockadesForEmptyJunction();
        if (getTrafficLight(roadsIn.get(0)).status() == TrafficLightStatus.GREEN_RED ||
                getTrafficLight(roadsIn.get(0)).status() == TrafficLightStatus.RED_GREEN ) {
            hasToWaitForEmptyJunction = true;
        }
        if(hasToWaitForEmptyJunction && junctionIsEmpty())
            hasToWaitForEmptyJunction = false;
        
        
        
    }
    private void updateBlockadesForEmptyJunction(){
        if (!junctionIsEmpty() && hasToWaitForEmptyJunction) {
            for (RoadSegment roadSegment : roadsIn) {
                BlockadeLocation bl = roadSegment.getNextDownstreamBlockade(0);
                bl.getBlockade().setState(true);
                //bl.getBlockade().setAlreadySetByAnotherCriticalInterval(true);
            }
            
        }
        else{
            for (RoadSegment roadSegment : roadsIn) {
                LaneSegment ls = roadSegment.laneSegment(1);
                if(ls.vehicles.isEmpty())
                {   
                    continue;
                }
                Vehicle veh = ls.getVehicle(0);
                LaneSegment lsToCheck = veh.route.get(veh.routeIndex+1).laneSegment(1);
                Vehicle vehOnInterval  = lsToCheck.isVehicleOnInterval(0, 30);
                if (vehOnInterval != null && (vehOnInterval.getAcc() < 0 || vehOnInterval.getSpeed()< 0.01)) {
                    BlockadeLocation bl = roadSegment.getNextDownstreamBlockade(0);
                    bl.getBlockade().setState(true);
                }
                else{
                    BlockadeLocation bl = roadSegment.getNextDownstreamBlockade(0);
                    bl.getBlockade().setState(false);
                    //bl.getBlockade().setAlreadySetByAnotherCriticalInterval(false);
                }
                
            }
        }
        
    }
    /*private boolean isDeadLockWhenTurningLeft(RoadSegment roadWithBlockade){
        ArrayList<RoadSegment> roadsWithBlockades = new ArrayList<>();

        int index = roadsTurningLeft.indexOf(roadWithBlockade);
        RoadSegment onComingLeftTurn = roadsTurningLeft.get((index+2)%4);
        if (roadWithBlockade.getVehicleCount()!=0) {
            if(roadWithBlockade.getVehicle(1, 0).getSpeed() < 0.01 && onComingLeftTurn.getVehicle(1, 0).getSpeed()< 0.01)
            {
                roadsWithBlockades.add(roadWithBlockade);
                roadsWithBlockades.add(onComingLeftTurn);
            }
        }
        
        if (roadsWithBlockades.size() != 0) {
            return false;
        } 
        return true;
    }*/
    
    /*private void resetBlockades(ArrayList<RoadSegment> roads){
        for (RoadSegment roadSegment : roads) {
            BlockadeLocation blockadeLoc = roadSegment.getNextDownstreamBlockade(0);
            blockadeLoc.getBlockade().setAlreadySetByAnotherCriticalInterval(false);
            blockadeLoc.getBlockade().setState(false);
        }
        
    }*/
    
    private boolean junctionIsEmpty(){
        for (RoadSegment roadSegment : roadsStreight) {
            if (roadSegment.getVehicleCount() != 0) {
                return false;
            }
        }
        for (RoadSegment roadSegment : roadsTurningLeft) {
            if (roadSegment.getVehicleCount() != 0) {
                return false;
            }
        }
        for (RoadSegment roadSegment : roadsTurningRight) {
            if (roadSegment.getVehicleCount() != 0) {
                return false;
            }
        }
        return true;
        
    }
    
    /*private boolean roadsDependingOnLeftTurnAreEmpty(RoadSegment leftTurnRoad){
        int index  = (roadsTurningLeft.indexOf(leftTurnRoad)+2)%4; //get roads streight and leftTurn of oncoming road
        RoadSegment roadStreight =  roadsStreight.get(index);
        RoadSegment roadRight = roadsTurningRight.get(index);
        RoadSegment roadIn = roadsIn.get(index);
        if (roadStreight.getVehicleCount() == 0 && 
                roadRight.getVehicleCount() == 0 
              && roadIn.getNumberOfVehiclesPastEnd() == 0 ) {
            return true;
        }
        return false;
    }*/
    /*private boolean roadsDependingOnLeftTurnAreEmpty(RoadSegment leftTurnRoad){
        int index  = (roadsTurningLeft.indexOf(leftTurnRoad)+2)%4; //get roads streight and leftTurn of oncoming road
        RoadSegment roadStreight =  roadsStreight.get(index);
        RoadSegment roadRight = roadsTurningRight.get(index);
        RoadSegment roadIn = roadsIn.get(index);
        LaneSegment laneSegmentOnComing = roadIn.laneSegment(1);
        Vehicle veh1 = laneSegmentOnComing.vehicles.get(0);
        if (roadStreight.getVehicleCount() == 0 && 
                roadRight.getVehicleCount() == 0 
              && roadIn.getNumberOfVehiclesPastEnd() == 0 && roadsTurningLeft.indexOf(veh1.route.get(veh1.routeIndex)) == -1) {
            return true;
        }
        return false;
    }*/
    /*private boolean checkIfDeadLock(RoadSegment roadTurningLeft){
        int index = (roadsTurningLeft.indexOf(roadTurningLeft));
        RoadSegment roadOnComming = roadsIn.get((index+2)%4);
        LaneSegment laneSegmentOnComing = roadOnComming.laneSegment(1);
        if( laneSegmentOnComing.vehicles.size() == 0)
            return false;
        Vehicle veh1 = laneSegmentOnComing.vehicles.get(0); 
        if (roadsTurningLeft.indexOf(veh1.route.get(veh1.routeIndex)) != -1 && veh1.getSpeed() < 2 ) {
            return true;
        }
        return false;
    }*/
    /* public void setBlocadesToPreventCrashing(){
        for (RoadSegment roadSegment : roads) {
            boolean blocadeNeeded = roadSegment.isVehicleOnInterval(5, 30);
            RoadSegment rs = getSinkRoadForTurningLeft(getOnComingRoad(roadSegment));
            BlockadeLocation blockadeLoc = rs.getNextDownstreamBlockade(0);
            if (Integer.parseInt(roadSegment.userId())%20 < 4  && blocadeNeeded) {//intervals hardcoded just for testing: ANDRAZ 
                --Vehicle obstacle = new Vehicle(rs.roadLength()/2, 0.0, Lanes.LANE1, 1.0, 1.0);
                obstacle.setType(Vehicle.Type.OBSTACLE);
                rs.addObstacle(obstacle);--
                Blockade blockade;
                if(blockadeLoc == null)
                    blockade = new Blockade(rs,rs.roadLength()/2);
                else
                    blockade = blockadeLoc.getBlockade();
                blockade.setState(true);
                try{
                    rs.addBlockadeLocation(new BlockadeLocation(rs.roadLength()/2,blockade));
                }catch(IllegalStateException e){
                    //System.out.println("WHEUIEFJK");
                }
            }else if (Integer.parseInt(roadSegment.userId())%20 < 4 && !blocadeNeeded && blockadeLoc != null){
                //removeObstacles(rs);
                //rs.removeBlockadeLocation(blockadeLoc);
                blockadeLoc.getBlockade().setState(false);
            }
                
        }
    }*/
    public RoadSegment getOnComingRoad(RoadSegment roadSegment){
        
        int roadSegmentID = ((int)Integer.parseInt(roadSegment.userId())) % 20;
        int onComingRoadId = ((roadSegmentID + 2) % 4 ) + (junctionID*20);
        for (RoadSegment road :roadsIn) {
            if(road.userId().equals(onComingRoadId+""))
            {
                return road;
            }
        }
        System.out.println("ERROR : CANT FIND A ROAD");
        return null;
    }
    public RoadSegment getSinkRoadForTurningLeft(RoadSegment roadSegment){
        int roadSegmentID = ((int)Integer.parseInt(roadSegment.userId())) % 20;
        int turnLeftID = 8 + roadSegmentID + (1 + (roadSegmentID*2)) + (junctionID*20);
        for (RoadSegment road : allRoads()) {
            if(road.userId().equals(turnLeftID+""))
            {
                return road;
            }
        }
        System.out.println("ERROR : CANT FIND A ROAD");
        return null;
    }
    
    public RoadSegment getSinkRoadForTurningRight(RoadSegment roadSegment){
        int roadSegmentID = ((int)Integer.parseInt(roadSegment.userId())) % 20;
        int turnLeftID = 8 + roadSegmentID + (2 + (roadSegmentID*2)) + (junctionID*20);
        for (RoadSegment road : allRoads()) {
            if(road.userId().equals(turnLeftID+""))
            {
                return road;
            }
        }
        return null;
    }
    public RoadSegment getSinkRoadStreight(RoadSegment roadSegment){
        int index = roadsIn.indexOf(roadSegment);
        return roadsStreight.get(index);
    }
    
   /* public void removeObstacles(RoadSegment roadSegment){
        for(LaneSegment ls : roadSegment.laneSegments()){
            ArrayList<Vehicle> clonedVehicles = (ArrayList<Vehicle>) ls.vehicles.clone();
            for(Vehicle veh : clonedVehicles){
                if(veh.type() == Type.OBSTACLE){
                    ls.vehicles.remove(veh);
                }
            }
        }
    }*/
    
    public void reset(){
        for (RoadSegment roadSegment : allRoads()) {
            BlockadeLocation blockadeLoc = roadSegment.getNextDownstreamBlockade(0);
            if (blockadeLoc != null) {
               blockadeLoc.reset();
               
            }
        }
    }
}
