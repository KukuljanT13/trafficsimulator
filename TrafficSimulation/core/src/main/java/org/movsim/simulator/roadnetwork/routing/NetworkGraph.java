/*
 * Copyright (C) 2010, 2011, 2012 by Arne Kesting, Martin Treiber, Ralph Germ, Martin Budden
 * <movsim.org@gmail.com>
 * -----------------------------------------------------------------------------------------
 * 
 * This file is part of
 * 
 * MovSim - the multi-model open-source vehicular-traffic simulator.
 * 
 * MovSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MovSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MovSim. If not, see <http://www.gnu.org/licenses/>
 * or <http://www.movsim.org>.
 * 
 * -----------------------------------------------------------------------------------------
 */
package org.movsim.simulator.roadnetwork.routing;

import java.time.Clock;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.movsim.simulator.roadnetwork.LaneSegment;
import org.movsim.simulator.roadnetwork.RoadNetwork;
import org.movsim.simulator.roadnetwork.RoadSegment;
import org.movsim.simulator.roadnetwork.RoadSegment.NodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class NetworkGraph implements Cloneable{

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(NetworkGraph.class);

    private static long vertexId = 0;

    private NetworkGraph() {
        // private constructor
    }

    public static WeightedGraph<Long, RoadSegment> create(RoadNetwork roadNetwork) {
        DefaultDirectedWeightedGraph<Long, RoadSegment> graph = new DefaultDirectedWeightedGraph<>(RoadSegment.class);
        for (RoadSegment roadSegment : roadNetwork) {

            Long fromVertex = getOrCreateVertex(NodeType.ORIGIN, roadSegment);
            Long toVertex = getOrCreateVertex(NodeType.DESTINATION, roadSegment);
            graph.addVertex(fromVertex);
            graph.addVertex(toVertex);
            graph.addEdge(fromVertex, toVertex, roadSegment);
            
            LOG.info("edge weight={}", graph.getEdgeWeight(roadSegment));
            
            graph.setEdgeWeight(roadSegment, roadSegment.roadLength());
            // add vertex to successor links AND to predecessor links of successors
            for (LaneSegment laneSegment : roadSegment.laneSegments()) {
                //Check the sinkLaneSegments and if they are not set, set their ORIGINs to the same as currentRoad segment DESTINATION
                if (laneSegment.sinkLaneSegment() != null) {
                    for(LaneSegment sinkLane : laneSegment.sinkLaneSegments){
                        RoadSegment successor = sinkLane.roadSegment();
                        if(successor.getNode(NodeType.ORIGIN) == null)
                            successor.setNode(NodeType.ORIGIN, toVertex);
                        for (LaneSegment laneSegmentSuccessor : successor.laneSegments()) {
                            if (laneSegmentSuccessor.sourceLaneSegment() != null) {
                                RoadSegment predecessor = laneSegmentSuccessor.sourceLaneSegment().roadSegment();
                                if(predecessor.getNode(NodeType.DESTINATION) == null)
                                    predecessor.setNode(NodeType.DESTINATION, toVertex);
                            }
                        }
                    }
                }
                //Check the sourceLaneSegments and if they are not set, set their DESTINATIONs to the same as currentRoad segment ORIGIN
                if (laneSegment.sourceLaneSegment() != null) {
                    for(LaneSegment sourceSink : laneSegment.sourceLaneSegments){
                        RoadSegment predecessor = sourceSink.roadSegment();
                        if(predecessor.getNode(NodeType.DESTINATION) == null)
                            predecessor.setNode(NodeType.DESTINATION, fromVertex);
                        for (LaneSegment laneSegmentSuccessor : predecessor.laneSegments()) {
                            if (laneSegmentSuccessor.sinkLaneSegment() != null) {
                                RoadSegment successor = laneSegmentSuccessor.sinkLaneSegment().roadSegment();
                                if(successor.getNode(NodeType.ORIGIN) == null)
                                    successor.setNode(NodeType.ORIGIN, fromVertex);
                            }
                        }
                    }
                }
                
            }
        }
        
        LOG.info("created graph with " + graph.edgeSet().size() + " edges and " + graph.vertexSet().size() + " nodes.");
        for (RoadSegment roadSegment : roadNetwork) {
            LOG.info(roadSegment.toString());
        }
        return graph;
    }
    
    private static long getOrCreateVertex(NodeType nodeType, RoadSegment roadSegment) {
        
        
        //Very very ugly coding. We should change that code to make it prettier
        Set<Long> vertices = new HashSet<Long>();
        Long v1 = roadSegment.getNode(nodeType);
        Long v2 = null;
        if(nodeType == NodeType.DESTINATION)
            v2 = roadSegment.getNodeIfAnyOfSilbingPredecessorsOrSuccessorsAreSet();
        if(nodeType == NodeType.ORIGIN)
            v2 = roadSegment.getNodeIfAnyOfSilbingSuccessorsOrPredecessorsAreSet();
        if(v1 != null)
            vertices.add(v1);
        if(v2 != null)
            vertices.add(v2);
        Long vertex = null;
        if (vertices.size() == 0) {
            vertex = vertexId++;
            roadSegment.setNode(nodeType, vertex);
        } else if(vertices.size() == 1){
            Iterator iterator = vertices.iterator();
            vertex = (Long) iterator.next();
        } else if(vertices.size() == 2){
            LOG.error("Incompatible vertices bettween roads. Cant create connection");
        }
        return vertex;
    }


}
