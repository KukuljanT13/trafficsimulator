/*
 * Copyright (C) 2010, 2011, 2012 by Arne Kesting, Martin Treiber, Ralph Germ, Martin Budden
 * <movsim.org@gmail.com>
 * -----------------------------------------------------------------------------------------
 * 
 * This file is part of
 * 
 * MovSim - the multi-model open-source vehicular-traffic simulator.
 * 
 * MovSim is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * MovSim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MovSim. If not, see <http://www.gnu.org/licenses/>
 * or <http://www.movsim.org>.
 * 
 * -----------------------------------------------------------------------------------------
 */
package org.movsim.utilities;

import java.util.Random;

/**
 * The Class MyRandom.
 */
public class MyRandom {

    private Random rand = new Random();

    public MyRandom(long randomSeed) {
        rand = new Random(randomSeed);
    }

    public void initializeWithSeed(long randomSeed) {
        rand = new Random(randomSeed);
    }

    public boolean isInitialized() {
        return rand != null;
    }

    /**
     * Next int.
     * 
     * @return the int
     */
    public int nextInt() {
        return rand.nextInt();
    }

    public int nextInt(int n) {
        return rand.nextInt(n);
    }

    /**
     * Next double.
     * 
     * @return the double
     */
    public double nextDouble() {
        return rand.nextDouble();
    }

    /**
     * returns a realization of a uniformly distributed random variable in [-1, 1]
     * 
     * @return a uniformly distributed realization in [-1, 1]
     */
    public double getUniformDistribution() {
        return 2 * nextDouble() - 1;
    }

    public double getUniformlyDistributedRandomizedFactor(double randomizationStrength) {
        return 1 + randomizationStrength * getUniformDistribution();
    }

    public double getGaussiansDistributedRandomizedFactor(double sigma, double nSigmaCutoff) {
        return 1 + Math.max(-nSigmaCutoff * sigma, Math.min(nSigmaCutoff, sigma * rand.nextGaussian()));
    }

}
