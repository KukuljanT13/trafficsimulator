package org.movsim.xml;

import com.google.common.base.Preconditions;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.movsim.autogen.Movsim;
import org.movsim.network.autogen.opendrive.OpenDRIVE;
import org.xml.sax.SAXException;

public class FileUnmarshaller<T> {

    private static final String W3C_XML_SCHEMA_NS_URI = "http://www.w3.org/2001/XMLSchema";

    /** Assure that only one loading/jaxb operation is active. */
    private static final Object SYNC_OBJECT = new Object();

    public final T load(StreamSource source, Class<T> clazz, Class<?> factory, URL xsdFile) throws JAXBException,
            SAXException,
            FileNotFoundException {
        T result;
        synchronized (SYNC_OBJECT) {
            // TODO creating a JaxbContext is expensive, consider pooling.
            Unmarshaller unmarshaller = createUnmarshaller(factory, xsdFile);
            unmarshaller.setEventHandler(new XmlValidationEventHandler());
            result = unmarshaller.unmarshal(source, clazz).getValue();
            /*Marshaller marshaller = createMarshaller(factory);
            OutputStream os = new FileOutputStream("nekejtakega.xprj");
            marshaller.marshal(result, os);*/
        }
        return result;
    }

    public static void save(Class<?> factory,OpenDRIVE openDriveNetwork, String fileName) throws JAXBException,
            SAXException,
            FileNotFoundException {
        synchronized (SYNC_OBJECT) {
            // TODO creating a JaxbContext is expensive, consider pooling.
        Marshaller marshaller = createMarshaller(factory);
        OutputStream os = new FileOutputStream("tojeto.xodr");

        marshaller.marshal(openDriveNetwork, os);
        }
    }
    public static void save(Class<?> factory,Movsim movsim, String fileName) throws JAXBException,
            SAXException,
            FileNotFoundException {
        synchronized (SYNC_OBJECT) {
            // TODO creating a JaxbContext is expensive, consider pooling.
        Marshaller marshaller = createMarshaller(factory);
        OutputStream os = new FileOutputStream("tojeto.xprj");

        marshaller.marshal(movsim, os);
        }
    }
//    public final T load(InputSource source, Class<T> clazz, Class<?> factory, URL xsdFile) throws JAXBException,
//            SAXException, ParserConfigurationException {
//        T result;
//        synchronized (SYNC_OBJECT) {
//            SAXParserFactory spf = SAXParserFactory.newInstance();
//            spf.setXIncludeAware(true);
//            spf.setNamespaceAware(true);
//            XMLReader xr = spf.newSAXParser().getXMLReader();
//            SAXSource src = new SAXSource(xr, source);
//            Unmarshaller unmarshaller = createUnmarshaller(factory, xsdFile);
//            unmarshaller.setEventHandler(new XmlValidationEventHandler());
//            result = unmarshaller.unmarshal(src, clazz).getValue();
//        }
//        return result;
//    }

    public final T load(File file, Class<T> clazz, Class<?> factory, URL xsdFile) throws JAXBException, SAXException, FileNotFoundException {
        Preconditions.checkNotNull(xsdFile);
        return load(new StreamSource(file), clazz, factory, xsdFile);
    }
    
//    public final T load(File file, Class<T> clazz, Class<?> factory, URL xsdFile) throws JAXBException, SAXException, ParserConfigurationException {
//        Preconditions.checkNotNull(file);
//        Preconditions.checkNotNull(xsdFile);
//        FileInputStream fileInputStream = null;
//        try {
//            fileInputStream = new FileInputStream(file);
//            InputSource inputSource = new InputSource(new InputStreamReader(fileInputStream));
//            inputSource.setEncoding("UTF-8");
//            return load(inputSource, clazz, factory, xsdFile);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    private final Unmarshaller createUnmarshaller(final Class<?> objectFactoryClass, final URL xsdFile)
            throws JAXBException, SAXException {
        JAXBContext context = JAXBContext.newInstance(objectFactoryClass);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        if (unmarshaller == null) {
            throw new JAXBException("Created unmarshaller is null.");
        }
        unmarshaller.setSchema(getSchema(xsdFile));
        return unmarshaller;
    }
    private static final Marshaller createMarshaller(final Class<?> objectFactoryClass) throws JAXBException, FileNotFoundException{
        JAXBContext context = JAXBContext.newInstance(objectFactoryClass);
        Marshaller marshaller = context.createMarshaller();
        return marshaller;
    }

    private static Schema getSchema(final URL xsdFile) throws SAXException {
        SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        return sf.newSchema(xsdFile);
    }

}
